//
//  MyOrderVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class MyOrderVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var popup_view: UIView!
    
    @IBOutlet weak var info_lbl: UILabel!
    
    
    
    @IBOutlet weak var grad_view: UIView!
    var selectIndex = -1
    
    @IBOutlet weak var order_table: UITableView!
    
    @IBAction func ok_act(_ sender: Any) {
        
        popup_view.isHidden = true

    }
    
    @IBAction func menu_act(_ sender: Any) {
        
        
        sideMenuController?.toggle()
        
    }
    
    
   var orderData = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        order_table.delegate = self
        order_table.dataSource = self
        

        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        
          //for setting gradient :
             
                        let gradientLayer: CAGradientLayer = CAGradientLayer()
                          gradientLayer.frame = view.bounds
                            let topColor: CGColor = themeColorFaint.cgColor
                            let middleColor: CGColor = themeColorDark.cgColor
                           let bottomColor: CGColor = themeColorDark.cgColor
                            gradientLayer.colors = [topColor, middleColor, bottomColor]
                               gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                              gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
             
                self.grad_view.clipsToBounds = true
                           self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        info_lbl.numberOfLines = 0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        myOrdersListing()
        
    }
    
    
    //MARK : TABLEVIEW METHODS:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! orderCell
        
        let dict = self.orderData[indexPath.row] as! NSDictionary
        
        
        let status = dict.value(forKey: "status") as! String
        let orderid = dict.value(forKey: "id") as! String

        cell.orderid_lbl.text = "Order Id: " + orderid
        
        
      if status == "0"
      {
        cell.status_lbl.text = "Pending"
        cell.status_lbl.textColor = thgreen
       // cell.status_lbl.alpha = 2

        cell.viewinvoice_btn.isHidden = true
        cell.viewinvoice_height.constant = 0

        }
        else if status == "1"
      {
        cell.status_lbl.text = "Approved"
        cell.status_lbl.textColor = .red

        cell.viewinvoice_btn.isHidden = false
              cell.viewinvoice_height.constant = 30

        }
        else if status == "2"
             {
               cell.status_lbl.text = "invoice"
                cell.status_lbl.textColor = thgreen
                cell.viewinvoice_btn.isHidden = false
                cell.viewinvoice_height.constant = 30


               } else if status == "3"
                    {
cell.status_lbl.text = "expired"
                        cell.status_lbl.textColor = .red

                      }
      else if status == "4"
                           {
cell.status_lbl.text = "Delivery Queue"
                            cell.status_lbl.textColor = thgreen
                            cell.viewinvoice_btn.isHidden = false
                            cell.viewinvoice_height.constant = 30

                             }
      else if status == "5"
                  {
cell.status_lbl.text = "In-transit"
                    cell.viewinvoice_btn.isHidden = false
                                 cell.viewinvoice_height.constant = 30
                    cell.status_lbl.textColor = thgreen
                    cell.viewinvoice_btn.isHidden = false
                    cell.viewinvoice_height.constant = 30
                    
                    //cell.status_lbl.alpha = 2


                    
        }
        else if status == "7"
                          {
        cell.status_lbl.text = "Returned"
                            cell.status_lbl.textColor = .red
                            
                            cell.viewinvoice_btn.isHidden = false
                            cell.viewinvoice_height.constant = 30

                }
        else if status == "8"
                                 {
               cell.status_lbl.text = "Delivered"
                                   cell.status_lbl.textColor = thgreen
                                    cell.viewinvoice_btn.isHidden = false
                                    cell.viewinvoice_height.constant = 30

                       }
        else if status == "9"
                                        {
                      cell.status_lbl.text = "Cancelled"
                    cell.status_lbl.textColor = .red

                   cell.viewinvoice_btn.isHidden = false
                                cell.viewinvoice_height.constant = 30
                                            cell.viewinvoice_btn.isHidden = false
                                            cell.viewinvoice_height.constant = 30
                                            
                                            
                                          
                              }
        
        
        
        cell.name_lbl.text = dict.value(forKey: "name") as! String
        cell.address_lbl.text = dict.value(forKey: "address1") as! String
        cell.number_lbl.text = dict.value(forKey: "mobile") as! String
        
        cell.info_btn.tag = indexPath.row
        cell.info_btn.addTarget(self, action: #selector(MyOrderVC.oninfo_btn(_:)), for: .touchUpInside)

        cell.viewDetail_btn.tag = indexPath.row
              cell.viewDetail_btn.addTarget(self, action: #selector(MyOrderVC.onviewDetail_btn(_:)), for: .touchUpInside)
        
        cell.viewinvoice_btn.tag = indexPath.row
              cell.viewinvoice_btn.addTarget(self, action: #selector(MyOrderVC.oninvoice_btn(_:)), for: .touchUpInside)
        

        
        cell.selectionStyle = .none
    
        
//        if indexPath.row == selectIndex
//            
//        {
//            
//            cell.backgroundColor = themeColorFaint.withAlphaComponent(0.5)
//        }
//        else
//            
//        {
//            cell.backgroundColor = UIColor.white.withAlphaComponent(1)
//        }
        
        
        return cell
    }
    @objc func oninvoice_btn(_ sender: UIButton?) {
        
        let index = sender?.tag
             
        let dict = self.orderData[index!] as! NSDictionary

             let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC

             vc.ordeDict = dict
             vc.stsnameString = dict.value(forKey: "status") as! String
        vc.camefromBtnStr =  "invoice"
        vc.selectedIndex = index!
             
             self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func onviewDetail_btn(_ sender: UIButton?) {
        
        selectIndex = sender!.tag
        let index = sender?.tag
             
        let dict = self.orderData[index!] as! NSDictionary

             let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC

             vc.ordeDict = dict
             vc.stsnameString = dict.value(forKey: "status") as! String
             vc.camefromBtnStr =  "detail"
        vc.selectedIndex = index!

             self.navigationController?.pushViewController(vc, animated: true)

    }

    @objc func oninfo_btn(_ sender: UIButton?) {

            print("Tapped")
        
        let index = sender?.tag
        
        
        let dict = self.orderData[index!] as! NSDictionary

        let status = dict.value(forKey: "status") as! String
              
             if status == "0"
             {
        
        info_lbl.text = "Pending order status means that your order is being reviewed and processed at your chosen pharmacy.Please wait for pharmacy approval.If you would like an update immediately,please call the pharmacy."
                popup_view.isHidden = false

        }
           else  if status == "1"
          {
                       
info_lbl.text = "Your order has been processed at the pharmacy and awaits payment.Once the payment has been proceesed,your order will be placed in the queue for delivery.Paymnets delayed past 30 mins will be automatically cancelled and will need to be re-ordered"
            popup_view.isHidden = false

                       }
                
                
       else if status == "9"
                    {
               
               info_lbl.text = "Your order has been cancelled."
               }
        
        
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        selectIndex = indexPath.row
//
//        let dict = self.orderData[indexPath.row] as! NSDictionary
//
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
//
//        vc.ordeDict = dict
//        vc.stsnameString = dict.value(forKey: "status") as! String
//
//        self.navigationController?.pushViewController(vc, animated: true)
//
        
        
        
        
        
        
}
    
 // MARK: - Webservice Calling

    func  myOrdersListing()
          {
               let type = NVActivityIndicatorType.ballClipRotateMultiple
                                      let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                      let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                             self.view.addSubview(activityIndicatorView)
                            self.view.isUserInteractionEnabled = false
               
              
                   activityIndicatorView.startAnimating()
                   var param = [String:Any]()
       
            param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                WebService().postRequest(methodName: myorder, parameter: param) { (response) in
                       
                       activityIndicatorView.stopAnimating()
                      self.view.isUserInteractionEnabled = true
                       
                       if let newResponse = response as? NSDictionary {
                           if newResponse.value(forKey: "status") as! Bool  == true {
                            
                            let userData = newResponse.value(forKey: "data") as! NSArray
                              
                             self.orderData = userData
                             
                             self.order_table.delegate = self
                             
                             self.order_table.dataSource = self
                             
                             self.order_table.reloadData()
                            
                               }
                        else {
                            
                    self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                      }
                           }
                        
                       else {
                          self.view.showToast(toastMessage: "No Data Found", duration: 1)

                       }
                   }
               
          }
    

}
