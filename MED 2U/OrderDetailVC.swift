//
//  OrderDetailVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import BraintreeDropIn
import Braintree
import MapKit


class OrderDetailVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate

{
    
    //MARK: IBOUTLETS :
    
    @IBOutlet weak var return_view: UIView!
    
    @IBOutlet weak var receivername_lbl: UILabel!
    
    @IBOutlet weak var receSign_image: UIImageView!
    
    @IBOutlet weak var driovername_lbl: UILabel!
    
    @IBOutlet weak var driverSign_image: UIImageView!
    
    @IBOutlet weak var returnvw_height: NSLayoutConstraint!
    
    
    
    
    
  //  @IBOutlet weak var scrollvw_height: NSLayoutConstraint!
    
    
    @IBOutlet weak var driverdetails_view: UIView!
    @IBOutlet weak var driverdetails_height: NSLayoutConstraint!
    @IBOutlet weak var driver_img: UIImageView!
    @IBOutlet weak var pickup_date: UILabel!
    @IBOutlet weak var driver_name: UILabel!
    @IBOutlet weak var driver_email: UILabel!
    @IBOutlet weak var driver_mobile: UILabel!
    @IBOutlet weak var map_view: MKMapView!
    
    
    
    
    var camefromBtnStr = String()
    var ordeDict = NSDictionary()
    
    var medicineDetailArr = NSArray()
    
    var stsString = String()
    var stsnameString = String()
    
    var tokenStr = String()

    
 var ststsArr = [String]()
    
    var selectedIndex = Int()
    
    var paymentMethodNonce = BTPaymentMethodNonce()
    var checkSatus = String()
    
    @IBOutlet weak var img_view: UIImageView!
    @IBOutlet weak var image_popup: UIView!
    @IBOutlet weak var bottonVW_height: NSLayoutConstraint!
    
@IBOutlet weak var subtoatl_lbl: UILabel!
    @IBOutlet weak var info_lbl: UILabel!
    
@IBOutlet weak var discount_lbl: UILabel!
    
@IBOutlet weak var deliCharges_lbl: UILabel!
    
@IBOutlet weak var ordertotal_lbl: UILabel!
    
    
    @IBOutlet weak var popup_view: UIView!
    
@IBOutlet weak var bottom_view: UIView!
    
@IBOutlet weak var custnumber: UILabel!
@IBOutlet weak var custname: UILabel!
    
@IBOutlet weak var custadd: UILabel!
@IBOutlet weak var pharmname_lbl: UILabel!
    
@IBOutlet weak var pharmaddress_lbl: UILabel!
    
@IBOutlet weak var pharmnumber_lbl: UILabel!
    
    
    
@IBOutlet weak var collection_vw: UICollectionView!
    
    @IBOutlet weak var grad_view: UIView!
    
    
    @IBOutlet weak var grad_vw: UIView!
    @IBOutlet weak var table_view: UITableView!
    
    @IBOutlet weak var table_height: NSLayoutConstraint!
    
    @IBOutlet weak var info_btn: UIButton!
    @IBOutlet weak var pay_btn: UIButton!
    
    @IBOutlet weak var bottom_space: NSLayoutConstraint!
    //MARK: IBACTIONS :
    
    
    //MARK: IBACTIONS :
    
    @IBAction func popupResign_act(_ sender: Any) {
        image_popup.isHidden = true
    }
    @IBAction func ok_act(_ sender: Any) {
        
        popup_view.isHidden = true

    }
    @IBAction func info_act(_ sender: Any) {
        
        
        popup_view.isHidden = false
        
        
    }
    @IBAction func pay_act(_ sender: Any) {
        
        
         if camefromBtnStr ==  "detail"
         {
            
            myOrdersListing()
            
            
//            if stsString == "3"
//            {
//              self.view.showToast(toastMessage: "Order Expired", duration: 1)
//            }
//            else
//            {
//            cancelOrderService()
//            }
        }
        else
         {
            
            myOrdersListing()
            
      //    showDropIn(clientTokenOrTokenizationKey: "fake-valid-nonce")
//           if stsString == "3"
//           {
//             self.view.showToast(toastMessage: "Order Expired", duration: 1)
//           }
//           else
//           {
//        showDropIn(clientTokenOrTokenizationKey: self.tokenStr)
//            }
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentConfirmationVC") as! PaymentConfirmationVC
//        self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func menu_act(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        //sideMenuController?.toggle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.driverdetails_height.constant = 0
        self.driverdetails_view.isHidden = true
        
        
        
        self.bottom_space.constant = 100
        
        self.returnvw_height.constant = 0
        self.return_view.isHidden = true
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        print("ordeDict",ordeDict)
        
        
        if camefromBtnStr ==  "detail"
        {
            
            self.pay_btn.setTitle("Cancel", for: .normal)
              
            self.info_btn.isHidden = false

//         let order_type_status = ordeDict.value(forKey: "order_type_status") as! String
//
//            if order_type_status == "1"
//            {
//              self.pay_btn.isHidden = true
//                self.info_btn.isHidden = false
//
//            }
//            else
//            {
//                self.pay_btn.isHidden = false
//                self.info_btn.isHidden = false
//
//
//            }
            
            
            
            
            self.bottom_view.isHidden = true
          bottonVW_height.constant = 0
        }
        else{
            
            gettingClienttoken()
            
            self.pay_btn.setTitle("Pay", for: .normal)
            self.info_btn.isHidden = true
            
           let payment_status = ordeDict.value(forKey: "payment_status") as! String

            if payment_status == "1"
            {
              self.pay_btn.isHidden = true
            }
            else
            {
                self.pay_btn.isHidden = false

            }
            
            
            
            self.bottom_view.isHidden = false
            bottonVW_height.constant = 128

        }
        
        
        

        
        
        
        medicineDetailArr = ordeDict.value(forKey: "medicine_detail") as! NSArray
        stsString = ordeDict.value(forKey: "status") as! String
        print("stsString",stsString)
        
        
                      if stsString == "0"
                      {
                        ststsArr = ["Pending","Approved","Delivery queue","In-transit","Delivered"]
                        
                   //     self.pay_btn.isHidden = false

                        
//        self.bottom_view.isHidden = true
//                        bottonVW_height.constant = 0

                        }
                        else if stsString == "1"
                      {
                        ststsArr = ["Pending","Approved","Delivery queue","In-transit","Delivered"]
                      //  self.pay_btn.isHidden = false

//        self.bottom_view.isHidden = true
//                        bottonVW_height.constant = 0


                        }
                        else if stsString == "2"
                             {
                             ststsArr = ["Pending","Approved","Delivery queue","In-transit","Delivered"]
                                
                            //    self.pay_btn.isHidden = false

//                                self.bottom_view.isHidden = true
//                                bottonVW_height.constant = 0

        
                               } else if stsString == "3"
                                    {
               ststsArr = ["Pending","Approved","expired","In-transit","Delivered"]
                                        
                                        
                                        
                                        

                                      //  self.bottom_view.isHidden = true

                                      }
                      else if stsString == "4"
                                           {
               ststsArr = ["Pending","Approved","Delivery queue","In-transit","Delivered"]
                                         //   self.pay_btn.isHidden = false

                                         //   self.bottom_view.isHidden = true

                                             }
                      else if stsString == "5"
                                  {
                ststsArr = ["Pending","Approved","Delivery queue","In-transit","Delivered"]
                                  //  self.pay_btn.isHidden = false
                                    self.driverdetails_height.constant = 380
                                                                       self.driverdetails_view.isHidden = false
                                                                       self.bottom_space.constant = 479
                                    self.IntransitService()
                                    
                                   

                                    if camefromBtnStr ==  "detail"
                                              {

                                    self.pay_btn.isHidden = false


                                              }
                                              else{
                                           self.pay_btn.isHidden = true
                                                                                        self.info_btn.isHidden = true


                                              }
                                       
                                    

       // self.bottom_view.isHidden = true

                        }
                        else if stsString == "7"
                                          {
                      ststsArr = ["Pending","Approved","Delivery queue","In-transit","Returned"]
                                            
                                            self.bottom_space.constant = 479

                                         self.returnvw_height.constant = 380
                                    self.return_view.isHidden = false

                                            self.ReturnService()
                                            
                                          
                                          //  self.pay_btn.isHidden = false

                                         //   self.bottom_view.isHidden = true
                                            
                                            self.pay_btn.isHidden = true
                                            self.info_btn.isHidden = true

        
                                }
                        else if stsString == "8"
                                                 {
                              ststsArr = ["Pending","Approved","Delivery queue","In-transit","Delivered"]
                                                    
                                                    
                                DeliveredService()
                                                    
                                                    
                                                 self.bottom_space.constant = 479

                                            self.returnvw_height.constant = 380
                                         self.return_view.isHidden = false
                                                    self.pay_btn.isHidden = true
                                                                                              self.info_btn.isHidden = true
        
                                       }
        
        else if stsString == "9"
                                                 {
                              ststsArr = ["Pending","Approved","Order Cancelled","In-transit","Delivered"]
                                                //    self.bottom_view.isHidden = true
                                                    self.pay_btn.isHidden = true
        self.info_btn.isHidden = true

                                       }
        
        
        
        
         
        

        let name = ordeDict.value(forKey: "name") as! String
       // custname.numberOfLines = 0
        
        custname.text =   name
        
        let address1 = ordeDict.value(forKey: "address1") as! String
        let address2 = ordeDict.value(forKey: "address2") as! String

          custadd.text = address1 + " , " + address2
        
        custnumber.text = ordeDict.value(forKey: "mobile") as! String
        
        let pharmname = ordeDict.value(forKey: "pharmecy_name") as! String
            pharmname_lbl.text =  pharmname
        pharmaddress_lbl.text = ordeDict.value(forKey: "pharmecy_address") as! String
       pharmnumber_lbl.text = ordeDict.value(forKey: "pharmecy_mobile") as! String
        
        subtoatl_lbl.text = ordeDict.value(forKey: "totalorder") as! String
        discount_lbl.text = ordeDict.value(forKey: "discount_amount") as! String
        deliCharges_lbl.text = ordeDict.value(forKey: "delivery_charge") as! String
        ordertotal_lbl.text = ordeDict.value(forKey: "grant_total") as! String
        
        
        
      

     
            self.map_view.delegate = self
        let lat = ordeDict.value(forKey: "latitude") as! String
               let long = ordeDict.value(forKey: "longitude") as! String
               let latitude =  Double(lat)
                                  let longitude = Double(long)
              
               
               self.map_view.isZoomEnabled = true
               
             let center = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
               let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
               self.map_view.setRegion(region, animated: true)

               let annotation = MKPointAnnotation()
               annotation.coordinate = center
               annotation.title = ordeDict.value(forKey: "pharmecy_address") as! String
            //   annotation.subtitle = "current location"
               self.map_view.addAnnotation(annotation)
        
     //   @IBOutlet weak var driver_img: UIImageView!
           
        
        
        
         
//
//     if stsnameString == "Delivered"
//{
//    self.bottom_view.isHidden = false
//}
//else
//{
//    self.bottom_view.isHidden = true
//
//        }
        
        
        collection_vw.delegate = self
        collection_vw.dataSource = self
        
        table_view.delegate = self
        table_view.dataSource = self
        
        if camefromBtnStr ==  "detail"
                     {
                         
             table_height.constant = CGFloat(180 * medicineDetailArr.count)

                     }
                     else{
                   
            table_height.constant = CGFloat(90 * medicineDetailArr.count)


                     }
        
        
         //for setting gradient :
            
                       let gradientLayer: CAGradientLayer = CAGradientLayer()
                         gradientLayer.frame = view.bounds
                           let topColor: CGColor = themeColorFaint.cgColor
                           let middleColor: CGColor = themeColorDark.cgColor
                          let bottomColor: CGColor = themeColorDark.cgColor
                           gradientLayer.colors = [topColor, middleColor, bottomColor]
                              gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                             gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
             self.grad_view.clipsToBounds = true
           self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
        self.pay_btn.clipsToBounds = true

                   self.pay_btn.layer.insertSublayer(gradientLayer, at: 0)
        
        self.grad_view.applyGradient(colours: [themeColorFaint, themeColorDark])
    }
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR",error)
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                let selectedPaymentOptionType = result.paymentOptionType
                let selectedPaymentMethod = result.paymentMethod
                let selectedPaymentMethodIcon = result.paymentIcon
                let selectedPaymentMethodDescription = result.paymentDescription
                
                
                self.paymentMethodNonce = result.paymentMethod!
                
                self.MakePayment()
                
                print("selectedPaymentOptionType",selectedPaymentOptionType)
                print("selectedPaymentMethod",selectedPaymentMethod as Any)
                print("selectedPaymentMethodIcon",selectedPaymentMethodIcon)
                print("selectedPaymentMethodDescription",selectedPaymentMethodDescription)

                
                
                
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if camefromBtnStr ==  "detail"
              {
                  
                        return 180

              }
              else{
            
                         return 90


              }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicineDetailArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! medicinesCell
        
        let dict = medicineDetailArr[indexPath.row] as! NSDictionary
        
        
       // let arr = dict.value(forKey: "medicine_detail") as! NSDictionary
        if camefromBtnStr ==  "detail"
                     {
                         
                              cell.medi_name.text = dict.value(forKey: "medicine_detail") as! String
                                      
                                      cell.medi_qty.text = ""
                                      
                                      cell.medi_price.text = ""
                        cell.img_vw.isHidden = false

                     }
                     else{
                   
                     cell.medi_name.text = "Medicine Detail"
                                       
                    cell.medi_qty.text = dict.value(forKey: "medicine_detail") as! String
                                       
              cell.medi_price.text = dict.value(forKey: "total_price") as! String
            cell.img_vw.isHidden = true


                     }
        
       
        
        let image = dict.value(forKey: "medicine_image") as! String
        
        if image == ""
        {
            
            cell.img_vw.image = UIImage.init(named:  "noimage")

        }
        else
        {
            cell.img_vw.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)

        }
        
        
        
       // cell.img_vw.image = UIImage.init(named: "info")
                   
        
        
      cell.selectionStyle = .none
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = medicineDetailArr[indexPath.row] as! NSDictionary

        let image = dict.value(forKey: "medicine_image") as! String

        
        img_view.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
                image_popup.isHidden = false

    }
    
    
    
    //MARK : COLLECTION VIEW METHOD :-
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ststsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! status_cell
        
        cell.stst_lbl.text = ststsArr[indexPath.row] as! String
        
        cell.sta_img.backgroundColor = .white
        
        if indexPath.row == 0
        {
            cell.linelead_lbl.isHidden = true
        }
        else if indexPath.row == self.ststsArr.count - 1
        {
           // cell.linelead_lbl.isHidden = true

            cell.lineend_lbl.isHidden = true

        }
        
        let sts = Int(stsString)
        
        if sts! <= 4
        {
        
            if sts == 3 || sts == 4
            {
                
                if  indexPath.row <= 2
                       {
                           cell.sta_img.image = UIImage.init(named: "selectedstatus")
                       }
                       else
                       {
                           cell.sta_img.image = UIImage.init(named: "unselect-status")
                
                       }
                
            }
            
       else
            {
            
        if  indexPath.row <= sts!
        {
            cell.sta_img.image = UIImage.init(named: "selectedstatus")
        }
        else
        {
            cell.sta_img.image = UIImage.init(named: "unselect-status")
 
        }
            }
        
        }
        
    else
        {
            
            if sts! == 5
            {
                 if  indexPath.row <= 3
                 {
                    cell.sta_img.image = UIImage.init(named: "selectedstatus")
                           }
                           else
                           {
                               cell.sta_img.image = UIImage.init(named: "unselect-status")
                    
                           }
            }
            else if sts! == 7 ||  sts! == 8
            {
                cell.sta_img.image = UIImage.init(named: "selectedstatus")
            }
            
            else if sts! == 9
            {
                 if  indexPath.row <= 2
                                {
                                   cell.sta_img.image = UIImage.init(named: "selectedstatus")
                                          }
                                          else
                                          {
                                              cell.sta_img.image = UIImage.init(named: "unselect-status")
                                   
                                          }
            }
                
                
                
            }
            
      
        
        

//              if status == "0"
//              {
//                cell.status_lbl.text = "Pending"
//
//                }
//                else if status == "1"
//              {
//                cell.status_lbl.text = "OrderPlaced"
//
//                }
//                else if status == "2"
//                     {
//                       cell.status_lbl.text = "invoice"
//
//                       } else if status == "3"
//                            {
//        cell.status_lbl.text = "expired"
//                              }
//              else if status == "4"
//                                   {
//        cell.status_lbl.text = "order_queue"
//                                     }
//              else if status == "5"
//                          {
//        cell.status_lbl.text = "accept_driver"
//
//                }
//                else if status == "7"
//                                  {
//                cell.status_lbl.text = "Return"
//
//                        }
//                else if status == "7"
//                                         {
//                       cell.status_lbl.text = "Delivered"
//
//                               }
//
    
        
        
        

        return cell
    }
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidthh = UIScreen.main.bounds.size.width
        let screenHt = UIScreen.main.bounds.size.height
        
        return CGSize(width: collectionView.frame.size.width/5  , height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    
    // MARK: - cancel Webservice Calling

       func  cancelOrderService()
             {
                  let type = NVActivityIndicatorType.ballClipRotateMultiple
                                         let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                         let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                self.view.addSubview(activityIndicatorView)
                               self.view.isUserInteractionEnabled = false
                  
                 
                      activityIndicatorView.startAnimating()
                      var param = [String:Any]()
          
               param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                param["order_id"] = ordeDict.value(forKey: "id") as! String

                
                
                   WebService().postRequest(methodName: cancel_order, parameter: param) { (response) in
                          
                          activityIndicatorView.stopAnimating()
                         self.view.isUserInteractionEnabled = true
                          
                          if let newResponse = response as? NSDictionary {
                              if newResponse.value(forKey: "status") as! Bool  == true {
                               
        self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)
                     
                                self.navigationController?.popViewController(animated: true)
                                

                               
                                  }
                           else {
                               
                       self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                         }
                              }
                           
                          else {
                             self.view.showToast(toastMessage: "No Data Found", duration: 1)

                          }
                      }
                  
             }
    func  IntransitService()
          {
               let type = NVActivityIndicatorType.ballClipRotateMultiple
                                      let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                      let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                             self.view.addSubview(activityIndicatorView)
                            self.view.isUserInteractionEnabled = false
               
              
                   activityIndicatorView.startAnimating()
                   var param = [String:Any]()
       
            param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

             
             
                WebService().postRequest(methodName: intransit_list, parameter: param) { (response) in
                       
                       activityIndicatorView.stopAnimating()
                      self.view.isUserInteractionEnabled = true
                       
                       if let newResponse = response as? NSDictionary {
                           if newResponse.value(forKey: "status") as! Bool  == true {
                            
                  let userData = newResponse.value(forKey: "data") as! NSArray

                    let mutArr = userData.mutableCopy() as! NSMutableArray
                            let orderid = self.ordeDict.value(forKey: "id") as! String
                            print("partorderid",orderid)
                            
                            let searchPredicate = NSPredicate(format: "id contains[C] %@",orderid)
                            let particularArray = mutArr.filtered(using: searchPredicate)
                            
                           let dataFiltered = (particularArray as NSArray).mutableCopy() as! NSMutableArray

                            print("particularArray",particularArray)
                            
                            
                            if particularArray.count != 0
                            {
                            
                            let pickupdate = dataFiltered.value(forKey: "delivery_date") as! NSArray
                            let driname = dataFiltered.value(forKey: "driver_name") as! NSArray
                            let driemail = dataFiltered.value(forKey: "driver_email") as! NSArray
                        let drimobile = dataFiltered.value(forKey: "driver_mobile") as! NSArray
                            
                            self.pickup_date.text = pickupdate[0] as! String
                                    self.driver_name.text = driname[0] as! String
                                    self.driver_email.text = driemail[0] as! String
                                      self.driver_mobile.text = drimobile[0] as! String
                                    
                                    let image = dataFiltered.value(forKey: "driver_image") as! NSArray
                            
                            let imgshow = image[0] as! String
                            
                    self.driver_img.af_setImage(withURL: URL(string: "\(img_Url)\(imgshow)")!)
                            }
                            else
                            {
                                
                                self.pickup_date.text = ""
                                self.driver_name.text = ""
                                self.driver_email.text = ""
                                self.driver_mobile.text = ""

                                self.view.showToast(toastMessage: "order id not present in list", duration: 1)

                                
                                
                            }
                            
                               }
                        else {
                            
               self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)


                                                      }
                           }
                        
                       else {
                          self.view.showToast(toastMessage: "No Data Found", duration: 1)

                       }
                   }
               
          }
    func  ReturnService()
            {
                 let type = NVActivityIndicatorType.ballClipRotateMultiple
                                        let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                        let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                               self.view.addSubview(activityIndicatorView)
                              self.view.isUserInteractionEnabled = false
                 
                
                     activityIndicatorView.startAnimating()
                     var param = [String:Any]()
         
              param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

               
               
                  WebService().postRequest(methodName: return_list, parameter: param) { (response) in
                         
                         activityIndicatorView.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                         
                         if let newResponse = response as? NSDictionary {
                             if newResponse.value(forKey: "status") as! Bool  == true {
                              
                    let userData = newResponse.value(forKey: "data") as! NSArray

                                      let mutArr = userData.mutableCopy() as! NSMutableArray
                                              let orderid = self.ordeDict.value(forKey: "id") as! String
                                              print("partorderid",orderid)

                                              let searchPredicate = NSPredicate(format: "id contains[C] %@",orderid)
                                              let particularArray = mutArr.filtered(using: searchPredicate)
                                              
                                             let dataFiltered = (particularArray as NSArray).mutableCopy() as! NSMutableArray

                                              print("particularArray",particularArray)
                                              if particularArray.count != 0
                                                                         {
                                       
                                let recname = dataFiltered.value(forKey: "name") as! NSArray
                                              self.receivername_lbl.text = recname[0] as! String
                                
                                
                                let driname = dataFiltered.value(forKey: "driver_name") as! NSArray
                                                                             self.driovername_lbl.text = driname[0] as! String
                                

                                                     let recimage = dataFiltered.value(forKey: "delivery_receiver_sign") as! NSArray
                                let recimgshow = recimage[0] as! String
//
                            self.receSign_image.af_setImage(withURL: URL(string: "\(img_Url)\(recimgshow)")!)
                              let driimage = dataFiltered.value(forKey: "delivery_driver_sign") as! NSArray
                                
                                let driimageshow = recimage[0] as! String

                                
                                self.driver_img.af_setImage(withURL: URL(string: "\(img_Url)\(driimageshow)")!)

                                }
                                else
                                   {
                                    self.receivername_lbl.text = ""
                                     self.driovername_lbl.text = ""
                             self.view.showToast(toastMessage: "order id not present in list", duration: 1)
                                                
                                }
                              
                                 }
                          else {
                              
                     
                                self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                        }
                             }
                          
                         else {
                            self.view.showToast(toastMessage: "No Data Found", duration: 1)

                         }
                     }
                 
            }
    func  DeliveredService()
                {
                     let type = NVActivityIndicatorType.ballClipRotateMultiple
                                            let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                            let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                   self.view.addSubview(activityIndicatorView)
                                  self.view.isUserInteractionEnabled = false
                     
                    
                         activityIndicatorView.startAnimating()
                         var param = [String:Any]()
             
                  param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

                   
                   
                      WebService().postRequest(methodName: delivered_list, parameter: param) { (response) in
                             
                             activityIndicatorView.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                             
                             if let newResponse = response as? NSDictionary {
                                 if newResponse.value(forKey: "status") as! Bool  == true {
                                  
                        let userData = newResponse.value(forKey: "data") as! NSArray

                                          let mutArr = userData.mutableCopy() as! NSMutableArray
                                                  let orderid = self.ordeDict.value(forKey: "id") as! String
                                                  print("partorderid",orderid)

                                                  let searchPredicate = NSPredicate(format: "id contains[C] %@",orderid)
                                                  let particularArray = mutArr.filtered(using: searchPredicate)
                                                  
                                                 let dataFiltered = (particularArray as NSArray).mutableCopy() as! NSMutableArray

                                                  print("particularArray",particularArray)
                                                  if particularArray.count != 0
                                                                             {
                                           
                                    let recname = dataFiltered.value(forKey: "name") as! NSArray
                                                  self.receivername_lbl.text = recname[0] as! String
                                    
                                    
                                    let driname = dataFiltered.value(forKey: "driver_name") as! NSArray
                                                                                 self.driovername_lbl.text = driname[0] as! String
                                    

                                                         let recimage = dataFiltered.value(forKey: "delivery_receiver_sign") as! NSArray
                                    let recimgshow = recimage[0] as! String
    //
                                self.receSign_image.af_setImage(withURL: URL(string: "\(img_Url)\(recimgshow)")!)
                                  let driimage = dataFiltered.value(forKey: "delivery_driver_sign") as! NSArray
                                    
                                    let driimageshow = recimage[0] as! String

                                    
                                    self.driver_img.af_setImage(withURL: URL(string: "\(img_Url)\(driimageshow)")!)

                                    }
                                    else
                                       {
                                        self.receivername_lbl.text = ""
                                         self.driovername_lbl.text = ""
                                 self.view.showToast(toastMessage: "order id not present in list", duration: 1)
                                                    
                                    }
                                  
                                     }
                              else {
                                  
                         
                                    self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                            }
                                 }
                              
                             else {
                                self.view.showToast(toastMessage: "No Data Found", duration: 1)

                             }
                         }
                     
                }
       
    // MARK: - Webservice Calling

              func  gettingClienttoken()
                    {
                         let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                       self.view.addSubview(activityIndicatorView)
                                      self.view.isUserInteractionEnabled = false
                         
                        
                             activityIndicatorView.startAnimating()
                             var param = [String:Any]()
                 
//                      param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
//
//
//                   param["latitude"] = "\(currentLoc.coordinate.latitude)"
//                       param["longitude"] = "\(currentLoc.coordinate.longitude)"

                             WebService().postRequest(methodName: clienttoken, parameter: param) { (response) in
                                 
                                 activityIndicatorView.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                                 
                                 if let newResponse = response as? NSDictionary {
                                     if newResponse.value(forKey: "status") as! Bool  == true {
                                      
                                        self.tokenStr = newResponse.value(forKey: "token") as! String
                                    
                                      
                                         }
                                  else {
                                      
                              self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                }
                                     }
                                  
                                 else {
                                    self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                 }
                             }
                         
                    }
              
  // For PAYMENT:
    
    
    
    func  MakePayment()
                        {
                             let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                    let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                    let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                           self.view.addSubview(activityIndicatorView)
                                          self.view.isUserInteractionEnabled = false
                             
                            
                                 activityIndicatorView.startAnimating()
                                 var param = [String:Any]()
                     
                          param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
 
    
                       param["payment_type"] = "braintree"
                            param["amount"] = self.ordertotal_lbl.text!
                            param["invoice_id"] = ordeDict.value(forKey: "invoice_id") as! String
                            param["order_id"] = ordeDict.value(forKey: "id") as! String
                            param["payment_method_nonce"] = "fake-valid-nonce"
                            //param["source"] = "Braintree"


                                 WebService().postRequest(methodName: payment, parameter: param) { (response) in
                                     
                                     activityIndicatorView.stopAnimating()
                                    self.view.isUserInteractionEnabled = true
                                     
                                     if let newResponse = response as? NSDictionary {
                                         if newResponse.value(forKey: "status") as! Bool  == true {
                                          
                                       self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)
                                            
                                            self.pay_btn.isHidden = true
                                          
                                             }
                                      else {
                                          
                                  self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                    }
                                         }
                                      
                                     else {
                                        self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                     }
                                 }
                             
                        }
       func  myOrdersListing()
                     {
                          let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                 let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                 let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                        self.view.addSubview(activityIndicatorView)
                                       self.view.isUserInteractionEnabled = false
                          
                         
                              activityIndicatorView.startAnimating()
                              var param = [String:Any]()
                  
                       param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                           WebService().postRequest(methodName: myorder, parameter: param) { (response) in
                                  
                                  activityIndicatorView.stopAnimating()
                                 self.view.isUserInteractionEnabled = true
                                  
                                  if let newResponse = response as? NSDictionary {
                                      if newResponse.value(forKey: "status") as! Bool  == true {
                                       
                                       let userData = newResponse.value(forKey: "data") as! NSArray
                                        
                                        let dict = userData[self.selectedIndex] as! NSDictionary
                                        
                                        self.checkSatus = dict.value(forKey: "status") as! String
                                        
                                        
                                        if self.camefromBtnStr ==  "detail"
                                                 {
                                                    
                                                    
                                                    if self.checkSatus == "3"
                                                    {
                                                      self.view.showToast(toastMessage: "Order Expired", duration: 1)
                                                    }
                                                    else
                                                    {
                                                        self.cancelOrderService()
                                                    }
                                                }
                                                else
                                                 {
                                                    
                                                    
                                                   if self.checkSatus == "3"
                                                   {
                                                     self.view.showToast(toastMessage: "Order Expired", duration: 1)
                                                   }
                                                   else
                                                   {
                                                    self.showDropIn(clientTokenOrTokenizationKey: self.tokenStr)
                                                    }
                                                
                                     
                                                }
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    
                                       
                                          }
                                   else {
                                       
                               self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                 }
                                      }
                                   
                                  else {
                                     self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                  }
                              }
                          
                     }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
