//
//  medicinesCell.swift
//  MED 2U
//
//  Created by CW-21 on 05/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

class medicinesCell: UITableViewCell {
    
    
    
    @IBOutlet weak var medi_name: UILabel!
    
    
    @IBOutlet weak var medi_qty: UILabel!
    

    @IBOutlet weak var medi_price: UILabel!
    
    
    @IBOutlet weak var img_vw: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
