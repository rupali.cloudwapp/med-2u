//
//  AppDelegate.swift
//  MED 2U
//
//  Created by CW-21 on 04/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GooglePlaces
import Braintree
import BraintreeDropIn
import FirebaseMessaging
import Firebase
import FirebaseAuth
import FirebaseCore
import FirebaseInstanceID

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       IQKeyboardManager.shared.enable = true
        GMSPlacesClient.provideAPIKey(google_APIkey)
        UIApplication.shared.statusBarStyle = .lightContent

      BTAppSwitch.setReturnURLScheme("Cloudwapp.MED-2U.payments")
// BTAppSwitch.setReturnURLScheme("Cloudwapp.MED-2U.payments")
        if #available(iOS 13.0, *){
                 
                          } else {
                          self.window?.makeKeyAndVisible()

                              
                          }
        FirebaseApp.configure()

         registerNotification(application)

               Auth.auth().languageCode = "En";

                 if #available(iOS 10.0, *) {
                         // For iOS 10 display notification (sent via APNS)
                         UNUserNotificationCenter.current().delegate = self
                         
                         let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                         UNUserNotificationCenter.current().requestAuthorization(
                             options: authOptions,
                             completionHandler: {_, _ in })
                         
                     } else {
                         let settings: UIUserNotificationSettings =
                             UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                         application.registerUserNotificationSettings(settings)
                     }
                     
                     
                  //   application.registerForRemoteNotifications()
                     Messaging.messaging().delegate = self
                     Messaging.messaging().isAutoInitEnabled = true

                      if #available(iOS 13.0, *){

                       } else {
                       self.window?.makeKeyAndVisible()


                       }
        let decviceId =  UIDevice.current.identifierForVendor!.uuidString
                    print("decviceId",decviceId)
                    UserDefaults.standard.set(decviceId, forKey: "decviceId")
        
        return true
    }
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
          
       
          
          if #available(iOS 13.0, *) {
            
            
                } else {
              
          
            
            if UserDefaults.standard.value(forKey: "userIdLogin") != nil
            {
                setHomeRootController()

            }
            else
            {

                setFirstRootController()
            }
        
            
            
          }
          
    
        
      return true
          
          
      }
    
    
    func setFirstRootController()  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var rootController = UIViewController()
        
        rootController = storyboard.instantiateViewController(withIdentifier: "FirstVC") as! FirstVC
        
        let navigationView = UINavigationController(rootViewController: rootController)
        navigationView.navigationBar.isHidden = true
        
        if let window = self.window {
            window.rootViewController = navigationView
        }
    }
    
    
    func setHomeRootController()  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var rootController = UIViewController()
        
        rootController = storyboard.instantiateViewController(withIdentifier: "CustomSideMenuController") as! CustomSideMenuController
        
        let navigationView = UINavigationController(rootViewController: rootController)
        navigationView.navigationBar.isHidden = true
        
        if let window = self.window {
            window.rootViewController = navigationView
        }
    }
    // MARK: UISceneSession Lifecycle
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        URLContexts.forEach { context in
            if context.url.scheme?.localizedCaseInsensitiveCompare("Cloudwapp.MED-2U.payments") == .orderedSame {
                BTAppSwitch.handleOpenURLContext(context)
            }
        }
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("Cloudwapp.MED-2U.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return false
    }
       func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
           // Called when a new scene session is being created.
           // Use this method to select a configuration to create the new scene with.
           return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
       }

       func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
           // Called when the user discards a scene session.
           // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
           // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
       }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    //MARK: AppNotification Method
    
    
    func registerNotification(_ application: UIApplication) {
         
         print("registerdnoticame")

         //Firebase
         if #available(iOS 10.0, *) {
             // For iOS 10 display notification (sent via APNS)
             UNUserNotificationCenter.current().delegate = self
             let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
             UNUserNotificationCenter.current().requestAuthorization(
                 options: authOptions,
                 completionHandler: {_, _ in
                    // print("registerdnoticame")
             }
             
             
             )
         } else {
             let settings: UIUserNotificationSettings =
                 UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
             application.registerUserNotificationSettings(settings)
         }
         UIApplication.shared.registerForRemoteNotifications()
         
         // FirebaseApp.configure()
     }
    
    
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            
            print("deviceToken.description",deviceToken.description)
            
            Auth.auth().setAPNSToken(deviceToken, type: .sandbox)
            Messaging.messaging().apnsToken = deviceToken
        }
        
//        func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//            if Auth.auth().canHandle(url) {
//                return true
//            }
//            return true
//        }
//
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
          InstanceID.instanceID().instanceID { (result, error) in
              if let error = error {
                  print("Error fetching remote instance ID: \(error)")
              } else if let result = result {
                  print("Remote instance ID token: \(result.token)")
                  let fcmToken = fcmToken
                
                
                  print("self.fcmTokenUser", fcmToken)
                
                UserDefaults.standard.set(fcmToken, forKey: "self.fcmToken")
                  
              }
          }
      }
        

    
      private func application(_ application: UIApplication,
                                 didReceiveRemoteNotification notification: [AnyHashable : Any],
                                 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
     
            
            if Auth.auth().canHandleNotification(notification) {
                            completionHandler(.noData)
                            return
                        }
            
            
      
            
            print("userinfo",notification)
            completionHandler(UIBackgroundFetchResult.newData)
        }
        
        // Firebase notification received
        func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
            completionHandler([.alert, .badge, .sound])
            // custom code to handle push while app is in the foreground
            print("Handle push from foreground, received: \n \(notification.request.content)")
            print(notification.request.content.userInfo)
            
            let type  = notification.request.content.userInfo[AnyHashable("gcm.notification.type")] as? String
            
            
            let userInfo = notification.request.content.userInfo
            
            
         
            
        }
    
        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                    withCompletionHandler completionHandler: @escaping () -> Void) {
            print("Handle tapped push from background, received: \n \(response.notification.request.content)")
            
          //  let type  = notification.request.content.userInfo[AnyHashable("gcm.notification.type")] as? String
            
            
        let userInfo = response.notification.request.content.userInfo
            
            
           
            
            
            completionHandler()
        }
    
        
    
        //MARK: messaging Delegate Method
      
        

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MED_2U")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

