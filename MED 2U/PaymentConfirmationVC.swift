//
//  PaymentConfirmationVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

class PaymentConfirmationVC: UIViewController {
    
    
    @IBOutlet weak var grad_view: UIView!
    
    @IBAction func menu_act(_ sender: Any) {
      //  sideMenuController?.toggle()
        self.navigationController?.popViewController(animated: true)
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }

        //for setting gradient :
             
                        let gradientLayer: CAGradientLayer = CAGradientLayer()
                          gradientLayer.frame = view.bounds
                            let topColor: CGColor = themeColorFaint.cgColor
                            let middleColor: CGColor = themeColorDark.cgColor
                           let bottomColor: CGColor = themeColorDark.cgColor
                            gradientLayer.colors = [topColor, middleColor, bottomColor]
                               gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                              gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
             
                self.grad_view.clipsToBounds = true
                           self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
