//
//  SidedrawerVC.swift
//  Dennish
//
//  Created by CW on 08/02/19.
//  Copyright © 2019 CW. All rights reserved.
//

import UIKit



@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SidedrawerVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
var menuArray = NSArray()
var imgArray = NSArray()
 var selectindex = -1
  
    @IBAction func profile_act(_ sender: Any) {
       
    }
    
    @IBOutlet weak var side_table: UITableView!
    
    
    //for PROFILE
    var parasedit = [String : String]()
    
    var datadictionary = NSDictionary()
    var indatadictionary = NSDictionary()
    var statustring = Bool()
    var text = NSString()
    var idtsrng = NSString()
    var emailstring = NSString()
    var dataArray = NSDictionary()
    let kPUserDefault = UserDefaults.standard
    var paraLg = [String : String]()
    
    let appDele = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
             menuArray = ["Home","My Profile","My Orders","Terms","Logout"]
             imgArray = ["home icon","profile icon","my order icon","terms icon","logout icon"]


        side_table.delegate = self
        side_table.dataSource = self

        
      //  ViewProfile()
        

        
       // self.imgArray = ["Home","logout"]
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
//   ViewProfile()
    //  name_lbl.text = UserDefaults.standard.value(forKey: "Loginname") as! String
        
   //  ViewProfile()
      
      NotificationCenter.default.addObserver(self, selector: #selector(methodprofile), name: Notification.Name("ViewProfile"), object: nil)
    }
    @objc func methodprofile(notification: NSNotification) {
      // ViewProfile()
        
        // Take Action on Notification
    }
  
    
    
    @IBOutlet weak var logo: UIImageView!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
   
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = side_table.dequeueReusableCell(withIdentifier: "cell") as! SideCellTableViewCell
       cell.selectionStyle = .none
        cell.lbl.text = menuArray[indexPath.row] as? String
        cell.lbl.textColor  = UIColor.darkGray
     cell.img.image = UIImage(named: imgArray[indexPath.row] as! String)
        
       
        if selectindex == indexPath.row {
            cell.backgroundColor = themeColorFaint.withAlphaComponent(0.2)
        } else {
            cell.backgroundColor = UIColor.clear
        }
        
        
//        let bgColorView = UIView()
//        bgColorView.backgroundColor = themeColorFaint
//        cell.selectedBackgroundView = bgColorView
        
        
        return cell
    }

    
    func Gologin()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var rootController = UIViewController()
        rootController = storyboard.instantiateViewController(withIdentifier: "loginVC") as! loginVC
        
        sideMenuController?.embed(centerViewController: rootController)
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
        
        selectindex = indexPath.row
        
        let cell = tableView.cellForRow(at: indexPath) as! SideCellTableViewCell
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = themeColorFaint
        cell.selectedBackgroundView = bgColorView
        
        
        switch (menuArray.object(at: indexPath.row) as? String) {
        case  "Home":
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
           sideMenuController?.embed(centerViewController: rootController)
            
           
        case  "My Profile":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC

            sideMenuController?.embed(centerViewController: rootController)
            
        case  "My Cart":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
            
            sideMenuController?.embed(centerViewController: rootController)
            
        case  "My Orders":
            
            UserDefaults.standard.set(false, forKey: "fromlogin/signup")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
            
            sideMenuController?.embed(centerViewController: rootController)
            
        case  "Payment":
            
            UserDefaults.standard.set(false, forKey: "fromlogin/signup")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            
            sideMenuController?.embed(centerViewController: rootController)
            
        case "Terms":
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            
            sideMenuController?.embed(centerViewController: rootController)
            
           
      
        case "Logout":
            
            let alert = UIAlertController(title: "LOGOUT ALERT", message: "Are you sure, You want logout?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Confirm", style: .default) { (action) in
               print("OK Pressed")
                              
                              
                            UserDefaults.standard.removeObject(forKey: "userIdLogin")
                              
                                  self.Gologin()
            }
            let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(okAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
            
           

          
        default:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "CustomSideMenuController")
            sideMenuController?.embed(centerViewController: rootController)

        }
    }
}
