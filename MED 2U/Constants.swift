//
//  Constants.swift
//  Teammates.net
//
//  Created by CP-02 on 05/12/18.
//  Copyright © 2018 CP-02. All rights reserved.
//

import Foundation
import AVKit




let google_APIkey = "AIzaSyDpLGc9pTTxvx4qAraD9bPV7oDgfBIpnj8"

let googleLanguage = "en"

let img_Url = "https://med2u.com.au/"
let base_url = "https://med2u.com.au/api/H1/"

let RegistrationService = "signup"
let LoginService = "login"
let logout = "logout"
let pharmacy_list = "pharmacy_list"
let get_detail = "get_detail"
let update_profile = "update_profile"
let order_text = "order_text"
let order_scan = "order_scan"
let terms_customer = "terms_customer"
let myorder = "myorder"
let cancel_order = "cancel_order"
let clienttoken = "clienttoken"
let payment = "payment"
let forgot_password = "forgot_password"
let intransit_list = "intransit_list"
let return_list = "return_list"
let delivered_list = "delivered_list"





//let themeColorFaint = hexStringToUIColor (hex:"8eb1cd")

let themeColorFaint = UIColor.init(red: 113/255.0, green: 145/255.0, blue: 166/255.0, alpha: 1)
let themeColorDark = hexStringToUIColor (hex:"2f5b7f")
let themeColorNavy = hexStringToUIColor (hex:"1B1F3D")


let thgreen = UIColor.init(red: 2/255.0, green: 162/255.0, blue: 95/255.0, alpha: 1)



func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
