//
//  CapturePrescriptionVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class CapturePrescriptionVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
    
    @IBOutlet weak var gard_view: UIView!
    
    
    //MARK : IBOUTLETS:
    
        var pahramcyIdStr = String()

    @IBOutlet weak var collection_view: UICollectionView!
    @IBOutlet weak var capture_btn: DesignableButton!
    
    
    
    @IBOutlet weak var submit_btn: UIButton!
    
    
    //MARK : IBACTIONS:
    
    
    @IBAction func Capture_act(_ sender: Any) {
        
        openFileAttachment ()
    }
    
    @IBAction func submit_act(_ sender: Any) {
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingDetailsVC") as! ShippingDetailsVC
                           vc.pahramcyIdStr =  pahramcyIdStr
                    vc.imgsArr = imgsArr
                           self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func menu_act(_ sender: Any) {
      //  sideMenuController?.toggle()
        self.navigationController?.popViewController(animated: true)
    }
    //MARK : IBDECLARATIONS:
    
    
    var imgsArr = [UIImage]()
    let picker = UIImagePickerController()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        picker.delegate = self
        collection_view.delegate = self
        collection_view.dataSource = self
    
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        //For setting grdaient :-
                                                 self.submit_btn.clipsToBounds = true

                                                        let gradientLayer: CAGradientLayer = CAGradientLayer()
                                                        gradientLayer.frame = view.bounds
                                                        let topColor: CGColor = themeColorFaint.cgColor
                                                        let middleColor: CGColor = themeColorDark.cgColor
                                                      let bottomColor: CGColor = themeColorDark.cgColor
                                                        gradientLayer.colors = [topColor, middleColor, bottomColor]
                                                        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                                        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                                                       // self.submit_btn.layer.insertSublayer(gradientLayer, at: 0)
        
             
                self.gard_view.clipsToBounds = true
                           self.gard_view.layer.insertSublayer(gradientLayer, at: 0)
        
        self.submit_btn.applyGradient(colours: [themeColorFaint, themeColorDark])

        
        
    }
    
    
    //MARK : COLLECTION VIEW METHOD :-
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PrescriptionCell
        
        cell.img_view.image = imgsArr[indexPath.row]
        cell.del_btn.tag = indexPath.row
        cell.del_btn.addTarget(self, action: #selector(buttonDelete), for: .touchUpInside)
        
        return cell
    }
     @objc func buttonDelete(sender:UIButton) {
        
        let indexPath = sender.tag
        
        imgsArr.remove(at: indexPath)
        
        collection_view.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidthh = UIScreen.main.bounds.size.width
        let screenHt = UIScreen.main.bounds.size.height
        
        return CGSize(width: collectionView.frame.size.width/2 - 10 , height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
         return 0
        
    }
    //METHOD FOR IMAGE PICKING:
    
    
    func openFileAttachment () {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title:"Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.openGallery()
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            picker.allowsEditing = false
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No Camera found in your Device", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        // present(picker, animated: true, completion: nil)
        
        self.present(picker, animated: true, completion: nil)
    }
    // MARK: - ImagePicker Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
           if imgsArr.count <= 3
            {
                imgsArr.append(pickedImage)

            }
            else
            {
                self.view.showToast(toastMessage: "You can not select more than 3 images", duration: 1)

            }
            
        }
        
        collection_view.reloadData()
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
