//
//  loginVC.swift
//  MED 2U
//
//  Created by CW-21 on 04/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class loginVC: UIViewController,UITextFieldDelegate {
    
    //MARK : IBOUTLETS :
    
    
    @IBOutlet weak var login_btn: UIButton!
    
       @IBOutlet weak var email_tf: UITextField!
    
    @IBOutlet weak var pw_tf: UITextField!
    
 //   @IBOutlet weak var email_tf: MDCTextField!
    
    @IBOutlet weak var eye_btn: UIButton!
    
    
    // MARK: Properties:
    
//    var emailIdController: MDCTextInputControllerOutlined?
//    var passwordController: MDCTextInputControllerOutlined?
//    var mobileNoController: MDCTextInputControllerOutlined?
    
    //MARK : IBACTIONS:

  
    @IBAction func login_act(_ sender: Any) {
        
        if !(email_tf.text!.isValidEmail()) {
           self.view.showToast(toastMessage:  "Valid Email Required!!", duration: 1)
                                                  }
              
               else if pw_tf.text!.isEmpty {
               
               self.view.showToast(toastMessage:  "Password Required!!", duration: 1)
               
               }
        else
        {
            signinUser()
        }
        
        
        
        
    }
    
    @IBAction func BACK(_ sender: Any) {
        
        
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "FirstVC") as! FirstVC
          
          self.navigationController?.pushViewController(vc, animated: true)
              }
      
    
    
    @IBAction func forgot_act(_ sender: Any) {
        
        
   

        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func eye_act(_ sender: Any) {
        
  if tag == 0
        {
            eye_btn.isSelected = true
            pw_tf.isSecureTextEntry = false
            tag = 1
            
        }
        else
  {
    eye_btn.isSelected = false

    pw_tf.isSecureTextEntry = true
    tag = 0

        }
        
        
        
    }
    //MARK: IBDELARATIONS:
    
 var tag = 0
   
  var appDele = UIApplication.shared.delegate as! AppDelegate
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.email_tf.textColor = .black
                           self.pw_tf.textColor = .black
                           
        
        
        
        
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
//     email_tf.text = "dk@hotmail.com"
//        pw_tf.text = "123456"
//        
//      
//        emailIdController = MDCTextInputControllerOutlined(textInput: email_tf)
//        
//        emailIdController!.inlinePlaceholderColor = .gray
//        emailIdController!.floatingPlaceholderActiveColor = .gray
//        emailIdController!.floatingPlaceholderNormalColor = themeColorNavy
//emailIdController?.borderStrokeColor = .gray
        
        
        
// emailIdController?.borderFillColor = .gray
        
        //For setting grdaient :-
                         
                                self.login_btn.clipsToBounds = true
                                let gradientLayer: CAGradientLayer = CAGradientLayer()
                                gradientLayer.frame = view.bounds
                                let topColor: CGColor = themeColorFaint.cgColor
                                let middleColor: CGColor = themeColorDark.cgColor
                              let bottomColor: CGColor = themeColorDark.cgColor
                                gradientLayer.colors = [topColor, middleColor, bottomColor]
                                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                                self.login_btn.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
    }
    
    func  signinUser()
       {
            let type = NVActivityIndicatorType.ballClipRotateMultiple
                                   let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                   let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                          self.view.addSubview(activityIndicatorView)
                         self.view.isUserInteractionEnabled = false
            
           
                activityIndicatorView.startAnimating()
                var param = [String:Any]()
      

        let fcmtoken = UserDefaults.standard.value(forKey: "self.fcmToken") as! String

        param["email"] = email_tf.text as! String
        param["password"] =  pw_tf.text as! String
        param["fcm_token"] = fcmtoken
        param["device_type"] = "IOS"
        param["device_id"] = UserDefaults.standard.value(forKey: "decviceId") as! String


                WebService().postRequest(methodName: LoginService , parameter: param) { (response) in
                    
                    activityIndicatorView.stopAnimating()
                   self.view.isUserInteractionEnabled = true
                    
                    if let newResponse = response as? NSDictionary {
                        if newResponse.value(forKey: "status") as! Bool  == true {
                            if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                                 
                           print("userData",userData)
                                
                                let userid = userData.value(forKey: "id") as! String
                
                       
                                
                                   
                                UserDefaults.standard.set(userid, forKey: "userIdLogin")
                            UserDefaults.standard.set(userData, forKey: "userData")
                                                                                                                       
                         if #available(iOS 13.0, *) {
                                                                                   

                                                             let scene = UIApplication.shared.connectedScenes.first
                                                  if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                    sd.setHomeRootController()
                                                                                     }
                                                        }
                                        else
                                                                                     
                            {
                                                                                             
                self.appDele.setHomeRootController()
                                                                                             
            }
                   
                                
                            }
                        }
                        else {
                            self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                        }
                    }
                    else if let newMsg = response as? String{
                       self.view.showToast(toastMessage: newMsg, duration: 1)
                    }
                    else {
                       self.view.showToast(toastMessage: "No Data Found", duration: 1)

                    }
                }
            
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
