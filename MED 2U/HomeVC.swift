//
//  HomeVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation

@available(iOS 13.0, *)
class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate {
    
    
    
    //MARK : IBOUTLETS:
    
    @IBOutlet weak var grad_view: UIView!
    @IBOutlet weak var Table_view: UITableView!
    
    @IBOutlet weak var welcome_lbl: UILabel!
    
      //MARK : IBACTIONS:
    
    @IBAction func menu_act(_ sender: Any) {
        sideMenuController?.toggle()
    }
    
    @IBAction func loc_act(_ sender: Any) {
    }
    //MARK : IBDECLARATIONS:
    
    var HomeData = NSArray()
    var latStr = String()
    var longStr = String()
    var locationManager = CLLocationManager()
    var currentLoc: CLLocation!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        //for setting gradient :
             
                        let gradientLayer: CAGradientLayer = CAGradientLayer()
                          gradientLayer.frame = view.bounds
                            let topColor: CGColor = themeColorFaint.cgColor
                            let middleColor: CGColor = themeColorDark.cgColor
                           let bottomColor: CGColor = themeColorDark.cgColor
                            gradientLayer.colors = [topColor, middleColor, bottomColor]
                               gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                              gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
             
                self.grad_view.clipsToBounds = true
                           self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
         
       
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        let userData = UserDefaults.standard.value(forKey: "userData") as! NSDictionary
                let name = userData.value(forKey: "name") as! String
        welcome_lbl.textAlignment = .center
        welcome_lbl.text =  "Welcome " + name + " ,here are nearest pharmacy based on your postal address."
        
    }
    //MARK: MAP-VIEW UPDATION METHOD:
      func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
          let locValue:CLLocationCoordinate2D = manager.location!.coordinate
          print("locations = \(locValue.latitude) \(locValue.longitude)")
          currentLoc = manager.location!
         PharmacyListing()
          locationManager.stopUpdatingLocation()


      }
    
    
    // MARK: - TABLE VIEW METHODS :-
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HomeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeCell
        
        cell.img_vw.contentMode = .scaleToFill
        cell.img_vw.image = UIImage.init(named: "pharmacy")
        cell.img_vw.backgroundColor = .white
        let dict = HomeData[indexPath.row] as! NSDictionary
        
        cell.name_lbl.text = dict.value(forKey: "pharmacy_name") as! String
        
    
        
       cell.selectionStyle = .none
        
        
        return cell
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderVC") as! PlaceOrderVC
        
             let dict = HomeData[indexPath.row] as! NSDictionary

                 vc.dict = dict
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    // MARK: - Webservice Calling

           func  PharmacyListing()
                 {
                      let type = NVActivityIndicatorType.ballClipRotateMultiple
                                             let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                             let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                    self.view.addSubview(activityIndicatorView)
                                   self.view.isUserInteractionEnabled = false
                      
                     
                          activityIndicatorView.startAnimating()
                          var param = [String:Any]()
              
                   param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                    
                    
                param["latitude"] = "\(currentLoc.coordinate.latitude)"
                    param["longitude"] = "\(currentLoc.coordinate.longitude)"

                          WebService().postRequest(methodName: pharmacy_list, parameter: param) { (response) in
                              
                              activityIndicatorView.stopAnimating()
                             self.view.isUserInteractionEnabled = true
                              
                              if let newResponse = response as? NSDictionary {
                                  if newResponse.value(forKey: "status") as! Bool  == true {
                                   
                                   let userData = newResponse.value(forKey: "data") as! NSArray
                                     
                                    self.HomeData = userData
                                    
                                    self.Table_view.delegate = self
                                    
                                    self.Table_view.dataSource = self
                                    
                                    self.Table_view.reloadData()
                                   
                                      }
                               else {
                                   
                           self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                             }
                                  }
                               
                              else {
                                 self.view.showToast(toastMessage: "No Data Found", duration: 1)

                              }
                          }
                      
                 }
           
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
