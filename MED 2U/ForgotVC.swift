//
//  ForgotVC.swift
//  MED 2U
//
//  Created by CW-21 on 04/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class ForgotVC: UIViewController {
    
    
    //MARK: IBACTION
    
    @IBOutlet weak var email_tf: UITextField!
    @IBAction func submit_act(_ sender: Any) {
      
        if !(email_tf.text!.isValidEmail()) {
                  self.view.showToast(toastMessage:  "Valid Email Required!!", duration: 1)
                                                         }
                   
               else
               {
                   forgot_passwordUser()
               }
    }
    
    @IBAction func BACK(_ sender: Any) {
                self.navigationController?.popViewController(animated: true)
            }
    
     //MARK: IBOUTLET
    
    @IBOutlet weak var submit_btn: UIButton!
    
    
    
    
     //MARK: IBDECARATION
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        //For setting grdaient :-
                               
                                      self.submit_btn.clipsToBounds = true
                                      let gradientLayer: CAGradientLayer = CAGradientLayer()
                                      gradientLayer.frame = view.bounds
                                      let topColor: CGColor = themeColorFaint.cgColor
                                      let middleColor: CGColor = themeColorDark.cgColor
                                    let bottomColor: CGColor = themeColorDark.cgColor
                                      gradientLayer.colors = [topColor, middleColor, bottomColor]
                                      gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                      gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                                      self.submit_btn.layer.insertSublayer(gradientLayer, at: 0)
        // Do any additional setup after loading the view.
    }
    
    func  forgot_passwordUser()
         {
              let type = NVActivityIndicatorType.ballClipRotateMultiple
                                     let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                     let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                            self.view.addSubview(activityIndicatorView)
                           self.view.isUserInteractionEnabled = false
              
             
                  activityIndicatorView.startAnimating()
                  var param = [String:Any]()
        



          param["email"] = email_tf.text as! String
          param["user_type"] =  "1"
         


                  WebService().postRequest(methodName: forgot_password , parameter: param) { (response) in
                      
                      activityIndicatorView.stopAnimating()
                     self.view.isUserInteractionEnabled = true
                      
                      if let newResponse = response as? NSDictionary {
                          if newResponse.value(forKey: "status") as! Bool  == true {
                              
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
                                    
                                    self.navigationController?.pushViewController(vc, animated: true)
                                                                                               
              }
                         else {
                                                      self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                  }
                          }
                         
                 
                      else if let newMsg = response as? String{
                         self.view.showToast(toastMessage: newMsg, duration: 1)
                      }
                      else {
                         self.view.showToast(toastMessage: "No Data Found", duration: 1)

                      }
                  }
              
         }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
