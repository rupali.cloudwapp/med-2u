//
//  SignupBackVC.swift
//  MED 2U
//
//  Created by CW-21 on 28/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SignupBackVC: UIViewController {
    
    // MARK: - IBOUTLETS
    
    
    @IBOutlet weak var state_tf: UITextField!
    
    @IBOutlet weak var pstcode_tf: UITextField!
    
    @IBOutlet weak var carename_tf: UITextField!
    
    @IBOutlet weak var carephone_tf: UITextField!
    
    
    @IBOutlet weak var secondAddress_tf: UITextField!
    
    @IBOutlet weak var doctorDetails_tf: UITextField!
    
    
     @IBOutlet weak var signup_btn: UIButton!
    
    // MARK: - IBACTIONS
    
    
    
    @IBAction func BACK(_ sender: Any) {
                  self.navigationController?.popViewController(animated: true)
              }
   
    
    @IBAction func signup_act(_ sender: Any) {
        
        
           if state_tf.text!.isEmpty {
              
              self.view.showToast(toastMessage:  "State Required!!", duration: 1)
              
              }
              else if pstcode_tf.text!.isEmpty {
              
              self.view.showToast(toastMessage:  "Postcode Required!!", duration: 1)
              
              }
             else if carename_tf.text!.isEmpty {
              
              self.view.showToast(toastMessage:  "Care Name Required!!", duration: 1)

              }
            else if carephone_tf.text!.isEmpty {
                         
                         self.view.showToast(toastMessage:  "Care Phone Required!!", duration: 1)

                         }
            else if secondAddress_tf.text!.isEmpty {
                         
                         self.view.showToast(toastMessage:  "Second Address Required!!", duration: 1)

                         }
            else if doctorDetails_tf.text!.isEmpty {
                         
                         self.view.showToast(toastMessage:  "Doctor Details Required!!", duration: 1)

                         }
           else
             {
          
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoVC") as! PersonalInfoVC
        vc.emailStr = emailStr
               vc.mobileStr = mobileStr
               vc.PwStr = PwStr
                vc.stateStr = state_tf.text as! String
               vc.pstcodeStr = pstcode_tf.text as! String
               vc.carenameStr = carename_tf.text as! String
        
        vc.carephoneStr = carephone_tf.text as! String
               vc.secondAddressStr = secondAddress_tf.text as! String
               vc.doctorDetailsStr = doctorDetails_tf.text as! String
        
          self.navigationController?.pushViewController(vc, animated: true)
        }
          
      }

    //MARK : IBDECLARATIONS:
        
       var emailStr = String()
       var mobileStr = String()
       var PwStr = String()
    var stateStr = String()
    var pstcodeStr = String()
    var carenameStr = String()
    var carephoneStr = String()
    var secondAddressStr = String()
    var doctorDetailsStr = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        self.state_tf.textColor = .black
               self.pstcode_tf.textColor = .black
               self.carename_tf.textColor = .black
        self.carephone_tf.textColor = .black
               self.secondAddress_tf.textColor = .black
        self.doctorDetails_tf.textColor = .black

        
        
        

        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        
        carephone_tf.keyboardType = .phonePad
         //For setting grdaient :-
               
                      self.signup_btn.clipsToBounds = true
                      let gradientLayer: CAGradientLayer = CAGradientLayer()
                      gradientLayer.frame = view.bounds
                      let topColor: CGColor = themeColorFaint.cgColor
                      let middleColor: CGColor = themeColorDark.cgColor
                    let bottomColor: CGColor = themeColorDark.cgColor
                      gradientLayer.colors = [topColor, middleColor, bottomColor]
                      gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                      gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                      self.signup_btn.layer.insertSublayer(gradientLayer, at: 0)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
