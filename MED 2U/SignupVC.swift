//
//  SignupVC.swift
//  MED 2U
//
//  Created by CW-21 on 04/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class SignupVC: UIViewController {
    
   
    //MARK : IBOUTLETS:
    
   var checktag = 0
    
    @IBOutlet weak var check_btn: UIButton!
    
    @IBOutlet weak var name_tf: UITextField!
    @IBOutlet weak var email_tf: UITextField!
    
    @IBOutlet weak var mobile_tf: UITextField!
    
    @IBOutlet weak var cpw_tf: UITextField!
    
    @IBOutlet weak var pweye_btn: UIButton!
    
    @IBOutlet weak var cpweye_btn: UIButton!
    @IBOutlet weak var PW_TF: UITextField!
    
    @IBOutlet weak var signup_btn: UIButton!
    //MARK : IBACTIONS:
    
    @IBAction func BACK(_ sender: Any) {
                  self.navigationController?.popViewController(animated: true)
              }
      
    
    @IBAction func check_act(_ sender: Any) {
        
        if checktag == 0
         {
             check_btn.isSelected = true
             checktag = 1
         }
        else
         {
            check_btn.isSelected = false
             checktag = 0
         }
    }
    
    

    @IBAction func terms_act(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
              
              self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func pweye_act(_ sender: Any) {
        
        if tag == 0
        {
            pweye_btn.isSelected = true
            PW_TF.isSecureTextEntry = false
            tag = 1
            
        }
        else
        {
            pweye_btn.isSelected = false
            
            PW_TF.isSecureTextEntry = true
            tag = 0
            
        }
    }
    
    @IBAction func cpweye_act(_ sender: Any) {
        
        if tag2 == 0
        {
            cpweye_btn.isSelected = true
            cpw_tf.isSecureTextEntry = false
            tag2 = 1
            
        }
        else
        {
            cpweye_btn.isSelected = false
            
            cpw_tf.isSecureTextEntry = true
            tag2 = 0
            
        }
    }
    
    @IBAction func signup_act(_ sender: Any) {
        if name_tf.text!.isEmpty {
        self.view.showToast(toastMessage:  "Name Required!!", duration: 1)
                                               }
        
      else if !(email_tf.text!.isValidEmail()) {
    self.view.showToast(toastMessage:  "Valid Email Required!!", duration: 1)
                                           }
        else if mobile_tf.text!.isEmpty {
        
        self.view.showToast(toastMessage:  "Mobile Required!!", duration: 1)
        
        }
        else if PW_TF.text!.isEmpty {
        
        self.view.showToast(toastMessage:  "Password Required!!", duration: 1)
        
        }
       else if PW_TF.text != cpw_tf.text {
        
        self.view.showToast(toastMessage:  "Confirm Password Not Matched!!", duration: 1)

        }
            else if checktag == 0
                   {
                       self.view.showToast(toastMessage:  "Please agree Terms & Conditions!!", duration: 1)

                   }
     else
       {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoVC") as! PersonalInfoVC
        
        vc.emailStr = email_tf.text as! String
        vc.mobileStr = mobile_tf.text as! String
        vc.PwStr = PW_TF.text as! String
        vc.nameStr = name_tf.text as! String
    
        self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    //MARK : IBDECLARATIONS:
    
    var tag = 0
    var tag2 = 0
    
    
    var emailStr = String()
    var mobileStr = String()
    var PwStr = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        
 
        
        self.name_tf.textColor = .black
        self.email_tf.textColor = .black
        self.mobile_tf.textColor = .black
        self.cpw_tf.textColor = .black
        self.PW_TF.textColor = .black

        
        
        
        
        
       // self.signup_btn.applyGradient(colours: [themeColorFaint, themeColorDark])
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
            
        
   //For setting grdaient :-
        
               self.signup_btn.clipsToBounds = true
               let gradientLayer: CAGradientLayer = CAGradientLayer()
               gradientLayer.frame = view.bounds
               let topColor: CGColor = themeColorFaint.cgColor
               let middleColor: CGColor = themeColorDark.cgColor
             let bottomColor: CGColor = themeColorDark.cgColor
               gradientLayer.colors = [topColor, middleColor, bottomColor]
               gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
               gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
               self.signup_btn.layer.insertSublayer(gradientLayer, at: 0)
         
    }
    
   
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
