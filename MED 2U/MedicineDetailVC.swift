//
//  MedicineDetailVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class MedicineDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //MARK : IBOUTLETS
    
    @IBOutlet weak var img_lbl: UILabel!
    
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var tyable_view: UITableView!
    
    @IBOutlet weak var desc_view: UIView!
    @IBOutlet weak var medicinename_lbl: UILabel!
    
    let picker = UIImagePickerController()

            var imgsArr = [UIImage]()
    
    var pickedImg = UIImage()

    
    
    @IBOutlet weak var descbx_height: NSLayoutConstraint!
    @IBOutlet weak var addStack_height: NSLayoutConstraint!
    
    @IBOutlet weak var table_height: NSLayoutConstraint!
    
    
    @IBOutlet weak var descrip_Image: UIImageView!
    
    @IBOutlet weak var grad_view: UIView!
    
    @IBOutlet weak var ADDMORE_BTN: UIButton!
    
    @IBOutlet weak var addImage_btn: UIButton!
    
    @IBOutlet weak var delete_btn: UIButton!
    
    
    @IBOutlet weak var submit_btn: UIButton!
    
    
    @IBOutlet weak var description_tv: UITextView!
    
    var medicine_detail = NSMutableArray()
    
    //MARK : IBACTIONS
    
    @IBAction func menu_act(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

       // sideMenuController?.toggle()
    }
    
    var addedorderMutableDict = [[String:Any]]()

    
    @IBAction func ADD_ACT(_ sender: Any) {
        
        img_lbl.isHidden = false

        self.delete_btn.isUserInteractionEnabled = true

        
        descbx_height.constant = 100
        
        
        
       /* if description_tv.text != ""
                      {
               
                       
                       if descrip_Image.image != nil
                       
                       {
                       let conviimage = self.descrip_Image.image!.compressTo(1)

                 let strBase64 = conviimage!.base64EncodedString(options: .lineLength64Characters)
                           let addedorderDict : [String:Any] =
                                                  [
                                                   "medicine_detail" : description_tv.text!,
                                                   
                                                   "medicine_image" :  strBase64


                                                  ]
                           addedorderMutableDict.append(addedorderDict)

                       }
                       else
                       {
                           
                           let addedorderDict : [String:Any] =
                                                                     [
                                                                      "medicine_detail" : description_tv.text!,
                                                                      
                                                                      "medicine_image" :  ""


                                                                     ]
                           addedorderMutableDict.append(addedorderDict)

                           
                       }

                 
                 
                            
                       
               }
               else
               {}*/
        
         
        
        if description_tv.text != ""
        {
                  if descrip_Image.image != nil
                  
                  {
                  let conviimage = self.descrip_Image.image!.compressTo(1)

            let strBase64 = conviimage!.base64EncodedString(options: .lineLength64Characters)
                      let addedorderDict : [String:Any] =
                                             [
                                              "medicine_detail" : description_tv.text!,
                                              
                                              "medicine_image" :  strBase64


                                             ]
                      addedorderMutableDict.append(addedorderDict)
                    let str = description_tv.text as! String
                                 
                                 medicine_detail.add(str)
                                 imgsArr.append(pickedImg)

                  }
                  else
                  {
                      
                      let addedorderDict : [String:Any] =
                                                                [
                                                                 "medicine_detail" : description_tv.text!,
                                                                 
                                                                 "medicine_image" :  ""


                                                                ]
                      addedorderMutableDict.append(addedorderDict)
                    let str = description_tv.text as! String
                                                    
                                                    medicine_detail.add(str)
                    imgsArr.append(UIImage())
                      
                  }
              // print("addedorderMutableDict",addedorderMutableDict)
               
             

        }
        else
        {
            self.view.showToast(toastMessage: "please add description", duration: 1)

        }
               
               delete_btn.isHidden = false
               addImage_btn.isHidden = false
               ADDMORE_BTN.isHidden = false
        

        self.addImage_btn.isUserInteractionEnabled = true

        
        self.desc_view.isHidden = false
            self.medicinename_lbl.isHidden = false

            
          
            descbx_height.constant = 120

        
        description_tv.text = ""
        descrip_Image.image = UIImage.init(named: "")

        
     tyable_view.delegate = self
        
            
            tyable_view.dataSource = self
        table_height.constant = CGFloat(140 * medicine_detail.count)

         scroll_view.contentSize.height = table_height.constant + 300
        
        tyable_view.reloadData()
               
        
    }
    
    @IBAction func AddImage_act(_ sender: Any) {
        
        openFileAttachment ()

    }
    
    
    @IBAction func deleteAct(_ sender: Any) {
        
        descbx_height.constant = 0
        
        
        self.desc_view.isHidden = true
        self.medicinename_lbl.isHidden = true

        
        delete_btn.isHidden = true
        addImage_btn.isUserInteractionEnabled = true
        ADDMORE_BTN.isHidden = false
        
        
       // addStack_height.constant = 0

//     if description_tv.text == ""
//     {
//        }
//        else
//     {
//     //   medicine_detail.remove(medicine_detail.count)
//
//        }
//
//    if  descrip_Image.image == nil
//    {
//
//        }
//        else
//    {
//
//        print(imgsArr.count)
//        //imgsArr.remove(at: imgsArr.count)
//
//        }
//
        
        
        description_tv.text = ""

        descrip_Image.image = UIImage.init(named: "")
        tyable_view.reloadData()
    }
    
    
    
    @IBAction func submit_act(_ sender: Any) {
        
        
        
        
        if description_tv.text != ""
               {
        
                
                if descrip_Image.image != nil
                
                {
                let conviimage = self.descrip_Image.image!.compressTo(1)

          let strBase64 = conviimage!.base64EncodedString(options: .lineLength64Characters)
                    let addedorderDict : [String:Any] =
                                           [
                                            "medicine_detail" : description_tv.text!,
                                            
                                            "medicine_image" :  strBase64


                                           ]
                    addedorderMutableDict.append(addedorderDict)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingDetailsVC") as! ShippingDetailsVC
                                       vc.pahramcyIdStr =  pahramcyIdStr
                                vc.addedorderMutableDict = addedorderMutableDict
                                       self.navigationController?.pushViewController(vc, animated: true)

                }
                else
                {
                    
                    let addedorderDict : [String:Any] =
                                                              [
                                                               "medicine_detail" : description_tv.text!,
                                                               
                                                               "medicine_image" :  ""


                                                              ]
                    addedorderMutableDict.append(addedorderDict)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingDetailsVC") as! ShippingDetailsVC
                                       vc.pahramcyIdStr =  pahramcyIdStr
                                vc.addedorderMutableDict = addedorderMutableDict
                                       self.navigationController?.pushViewController(vc, animated: true)

                    
                }
             
                
        }
        else
        {
            
            self.view.showToast(toastMessage: "please add description", duration: 1)

            
        }
        

       
      //  }
        
        
    }
    //MARK : IBDECLARATIONS
    var dictionaries = [[String:String]]()

    var pahramcyIdStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        picker.delegate = self
        picker.allowsEditing = true
        
        medicine_detail.removeAllObjects()
        imgsArr.removeAll()
               table_height.constant = 0
        
        
        
         
        
        //For setting grdaient :-
                                           self.submit_btn.clipsToBounds = true

                                                  self.ADDMORE_BTN.clipsToBounds = true
                                                  let gradientLayer: CAGradientLayer = CAGradientLayer()
                                                  gradientLayer.frame = view.bounds
                                                  let topColor: CGColor = themeColorFaint.cgColor
                                                  let middleColor: CGColor = themeColorDark.cgColor
                                                let bottomColor: CGColor = themeColorDark.cgColor
                                                  gradientLayer.colors = [topColor, middleColor, bottomColor]
                                                  gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                                  gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                                                  self.ADDMORE_BTN.layer.insertSublayer(gradientLayer, at: 0)
        
        self.ADDMORE_BTN.applyGradient(colours: [themeColorFaint, themeColorDark])
        
        self.addImage_btn.applyGradient(colours: [themeColorFaint, themeColorDark])
        self.delete_btn.applyGradient(colours: [themeColorFaint, themeColorDark])
        
      
       // self.ADDMORE_BTN.isUserInteractionEnabled = false
        self.addImage_btn.isUserInteractionEnabled = true
          self.delete_btn.isUserInteractionEnabled = false
        
 
        self.description_tv.tintColor = themeColorNavy
        self.description_tv.textColor = themeColorNavy

      // self.description_tv.text =  "hii"
       self.submit_btn.applyGradient(colours: [themeColorFaint, themeColorDark])
             
       self.grad_view.clipsToBounds = true
        
     self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
         
        // Do any additional setup after loading the view.
    }
    class func convertImageToBase64(image: UIImage) -> String {
           let imageData = UIImagePNGRepresentation(image)!
           return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
       }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
       

    }
    
    override func viewDidAppear(_ animated: Bool) {
        scroll_view.contentSize.height = table_height.constant + 200
    }
    
    //MARK TABLEVIEW METHODS :
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicine_detail.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MedidetailCell
        
        
        
        cell.detail_tv.textColor = .black
        
        if medicine_detail.count != 0
        
        {
        cell.detail_tv.text = medicine_detail[indexPath.row] as! String
        }
        else
        {}
        
        if imgsArr.count != 0
        
        {
        let image = imgsArr[indexPath.row] as! UIImage
            cell.description_image.image = image

        }
        else
               {}
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    //METHOD FOR IMAGE PICKING:
    
    
    func openFileAttachment () {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title:"Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.openGallery()
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            picker.allowsEditing = false
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No Camera found in your Device", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        // present(picker, animated: true, completion: nil)
        
        self.present(picker, animated: true, completion: nil)
    }
    // MARK: - ImagePicker Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
          
               // imgsArr.append(pickedImage)
            
            pickedImg = pickedImage
            self.descrip_Image.image = pickedImage
            
            img_lbl.isHidden = true
        }
         
        tyable_view.reloadData()
        
        
        
        self.ADDMORE_BTN.isUserInteractionEnabled = true
               self.addImage_btn.isUserInteractionEnabled = true
               self.delete_btn.isUserInteractionEnabled = true
        
        picker.dismiss(animated: true, completion: nil)
    }
        
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
