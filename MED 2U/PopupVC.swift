//
//  PopupVC.swift
//  MED 2U
//
//  Created by CW-21 on 05/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class PopupVC: UIViewController {
    
    
    //MARK : IBOPUTLETS:
    
    
    @IBOutlet weak var GRADIENT_VIEW: UIView!
    
    
    @IBOutlet weak var signup_bth: UIButton!
    
    
    
    //MARK : IBACTIONS:
    
    
    
    @IBAction func Signup_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
  
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
       if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
           
           let statusbarView = UIView()
           statusbarView.backgroundColor = themeColorFaint
           view.addSubview(statusbarView)
         
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
               .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
               .constraint(equalTo: view.topAnchor).isActive = true
           statusbarView.centerXAnchor
               .constraint(equalTo: view.centerXAnchor).isActive = true
         
       } else {
           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
           statusBar?.backgroundColor = themeColorFaint
       }
        
        
        self.signup_bth.applyGradient(colours: [themeColorFaint, themeColorDark])
        self.GRADIENT_VIEW.applyGradient(colours: [themeColorFaint, themeColorDark])


        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
