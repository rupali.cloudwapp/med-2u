//
//  PlaceOrderVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class PlaceOrderVC: UIViewController {
    
    //MARK := IBOUTLETS:
    
    
    @IBOutlet weak var grad_view: UIView!
    @IBOutlet weak var pharmacyName_lbl: UILabel!
    @IBOutlet weak var pharmacyName_tf: DesignableView!

    
    
    @IBOutlet weak var pharmacyAddress_lbl: UILabel!
    
    @IBOutlet weak var pharmacyNumber_lbl: UILabel!
    
    @IBOutlet weak var ordertimes_lbl: UILabel!
    
     //MARK := IBACTIONS:
    
    
    @IBAction func MENU_ACT(_ sender: Any) {
     
        self.navigationController?.popViewController(animated: true)

       // sideMenuController?.toggle()
    }
    
    
    @IBAction func txt_act(_ sender: Any) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MedicineDetailVC") as! MedicineDetailVC
        vc.pahramcyIdStr =  dict.value(forKey: "id") as! String
        UserDefaults.standard.set(true, forKey: "fromTxt")
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func uploads_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CapturePrescriptionVC") as! CapturePrescriptionVC
        vc.pahramcyIdStr =  dict.value(forKey: "id") as! String
        UserDefaults.standard.set(false, forKey: "fromTxt")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func call_act(_ sender: Any) {
        
      let number = dict.value(forKey: "pharmacy_contact") as! String

        if let url = URL(string: "tel://\(number)"),
                   UIApplication.shared.canOpenURL(url) {
                   if #available(iOS 10, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler:nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               } else {
            self.view.showToast(toastMessage: "Call error", duration: 1)

               }

        
    }
    
    
    
    
      //MARK := IBDECLARATIONS:
    
  var  dict = NSDictionary()
    var pahramcyIdStr = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
        
        //for setting gradient :
             
                        let gradientLayer: CAGradientLayer = CAGradientLayer()
                          gradientLayer.frame = view.bounds
                            let topColor: CGColor = themeColorFaint.cgColor
                            let middleColor: CGColor = themeColorDark.cgColor
                           let bottomColor: CGColor = themeColorDark.cgColor
                            gradientLayer.colors = [topColor, middleColor, bottomColor]
                               gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                              gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
             
                self.grad_view.clipsToBounds = true
                           self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
        
        
        
        
let close_monday_friday = dict.value(forKey: "close_monday_friday") as! String
      let  close_saturday = dict.value(forKey: "close_saturday") as! String
let close_sunday = dict.value(forKey: "close_sunday") as! String
        
        let start_monday_friday = dict.value(forKey: "start_monday_friday") as! String
              let  start_saturday = dict.value(forKey: "start_saturday") as! String
        let start_sunday = dict.value(forKey: "start_sunday") as! String
                
        
        ordertimes_lbl.text = "Monday - Friday :" + start_monday_friday + " - " + close_monday_friday  + "\n" + "Saturday :" + start_saturday + " - " + close_saturday + "\n" +  "Sunday :" + start_sunday + " - " + close_sunday
        
               
    self.pharmacyName_lbl.text = dict.value(forKey: "pharmacy_name") as! String
        
         self.pharmacyNumber_lbl.text = dict.value(forKey: "pharmacy_contact") as! String
        
        self.pharmacyAddress_lbl.text = dict.value(forKey: "address1") as! String

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
