//
//  ProfileVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AlamofireImage
import GooglePlaces
import NVActivityIndicatorView
import MKDropdownMenu

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class ProfileVC: UIViewController {
    
        
     var tag = 0
       
       
       var nameStr = String()

     var genderArr = ["Male","Female"]
       
       var priAddressbool = Bool()
       var secAddressbool = Bool()

       var placesClient = GMSPlacesClient()
       
        //MARK : IBOUTLETS:
       
    @IBOutlet weak var email_tf: UITextField!
    @IBOutlet weak var mobile_tf: UITextField!
    
    
       @IBOutlet weak var gender_dropdown: MKDropdownMenu!
       @IBOutlet weak var gender_tf: UITextField!
       @IBOutlet weak var Dob_tf: UITextField!
       @IBOutlet weak var addressLine1_tf: UITextField!
       @IBOutlet weak var addressline2_tf: UITextField!
       @IBOutlet weak var state_tf: UITextField!
       @IBOutlet weak var postcode_tf: UITextField!
       @IBOutlet weak var secAddressline1_tf: UITextField!
       @IBOutlet weak var mediCrdNumber_tf: UITextField!
       @IBOutlet weak var mediExpiry_tf: UITextField!
       @IBOutlet weak var concCrd_tf: UITextField!
       @IBOutlet weak var consExpiry_tf: UITextField!
       @IBOutlet weak var anyother_tf: UITextField!
       @IBOutlet weak var docName_tf: UITextField!
       @IBOutlet weak var docPhone_tf: UITextField!
       @IBOutlet weak var mediHistory_tf: UITextField!
       @IBOutlet weak var allergies_tf: UITextField!
       @IBOutlet weak var suburb_tf: UITextField!
       @IBOutlet weak var name_lbl: UILabel!
       @IBOutlet weak var signup_buton: DesignableButton!

       @IBOutlet weak var pro_img: UIImageView!
       
       @IBOutlet weak var gradient_view: UIView!
       @IBOutlet weak var datePicker: UIDatePicker!

       @IBOutlet weak var timedate_view: UIView!
        @IBOutlet weak var secsuburbn_tf: UITextField!
       @IBOutlet weak var secstate_tf: UITextField!
       @IBOutlet weak var secpostcode_tf: UITextField!
       @IBOutlet weak var carername_tf: UITextField!
       @IBOutlet weak var carerphone_tf: UITextField!
        @IBOutlet weak var check_btn: UIButton!
       
       
       
       //MARK : IBACTIONS:
    
    
    
    
    @IBAction func edit_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
       @IBAction func secaddress1_act(_ sender: Any) {
           
         
           
       }
       
       
       @IBAction func check_act(_ sender: Any) {
         
           
       }
       
       
       
       
       
       
       
       @IBAction func dob_act(_ sender: Any) {
         

       }
       @IBAction func done_act(_ sender: Any) {
           
          


       }
       @IBAction func cancel_act(_ sender: Any) {
           
       }
       
       @IBAction func consExp_act(_ sender: Any) {
           
       }
       
       @IBAction func mediExp_act(_ sender: Any) {
         
       }
       @IBAction func address1_act(_ sender: Any) {
           
   
       }
       
       @IBAction func signup_act(_ sender: Any) {
           
          
       }
       
       @IBAction func pro_act(_ sender: Any) {
           
       }
       
       @IBAction func BACK(_ sender: Any) {
        sideMenuController?.toggle()
            }
       
       
      //MARK : IBDECLARATIONS:
       
       var appDele = UIApplication.shared.delegate as! AppDelegate

        var topassLat = String()
        var topassLong = String()
       var sectopassLat = String()
       var sectopassLong = String()
       
       var mediExpirybool = Bool()
         var consExpirybool = Bool()
       var dobbool = Bool()
         
       var imgStr = String()
             var emailStr = String()
                   var mobileStr = String()
                   var PwStr = String()
                var stateStr = String()
                var pstcodeStr = String()
                var carenameStr = String()
                var carephoneStr = String()
                var secondAddressStr = String()
                var doctorDetailsStr = String()
       
     let picker = UIImagePickerController()
       
       
       
       
         
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            self.gender_tf.textColor = .black
                             self.Dob_tf.textColor = .black
                             self.addressLine1_tf.textColor = .black
                      self.addressline2_tf.textColor = .black
                             self.state_tf.textColor = .black
                      self.postcode_tf.textColor = .black
                
                self.secAddressline1_tf.textColor = .black
                                    self.mediCrdNumber_tf.textColor = .black
                                    self.mediExpiry_tf.textColor = .black
                             self.concCrd_tf.textColor = .black
                                    self.consExpiry_tf.textColor = .black
                             self.anyother_tf.textColor = .black
                
                self.docName_tf.textColor = .black
                                           self.docPhone_tf.textColor = .black
                                           self.mediHistory_tf.textColor = .black
                                    self.allergies_tf.textColor = .black
                                           self.suburb_tf.textColor = .black
                                    self.secsuburbn_tf.textColor = .black
            
            
            
            
            
            
            if #available(iOS 13.0, *) {
                      let app = UIApplication.shared
                      let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                      
                      let statusbarView = UIView()
                      statusbarView.backgroundColor = themeColorFaint
                      view.addSubview(statusbarView)
                    
                      statusbarView.translatesAutoresizingMaskIntoConstraints = false
                      statusbarView.heightAnchor
                          .constraint(equalToConstant: statusBarHeight).isActive = true
                      statusbarView.widthAnchor
                          .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                      statusbarView.topAnchor
                          .constraint(equalTo: view.topAnchor).isActive = true
                      statusbarView.centerXAnchor
                          .constraint(equalTo: view.centerXAnchor).isActive = true
                    
                  } else {
                      let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                      statusBar?.backgroundColor = themeColorFaint
                  }
            
            //For setting grdaient :-
                                          
                                                 self.signup_buton.clipsToBounds = true
                                                 let gradientLayer: CAGradientLayer = CAGradientLayer()
                                                 gradientLayer.frame = view.bounds
                                                 let topColor: CGColor = themeColorFaint.cgColor
                                                 let middleColor: CGColor = themeColorDark.cgColor
                                               let bottomColor: CGColor = themeColorDark.cgColor
                                                 gradientLayer.colors = [topColor, middleColor, bottomColor]
                                                 gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                                 gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                                                 self.signup_buton.layer.insertSublayer(gradientLayer, at: 0)
            
            self.gradient_view.clipsToBounds = true
                                                     
                                                      self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
            
            self.signup_buton.isHidden = true
            
            
            self.state_tf.isUserInteractionEnabled = false
            self.postcode_tf.isUserInteractionEnabled = false
            self.Dob_tf.isUserInteractionEnabled = false
            self.addressLine1_tf.isUserInteractionEnabled = false
            self.addressline2_tf.isUserInteractionEnabled = false
          
            self.mediCrdNumber_tf.isUserInteractionEnabled = false
            self.mediExpiry_tf.isUserInteractionEnabled = false
          self.concCrd_tf.isUserInteractionEnabled = false
          self.consExpiry_tf.isUserInteractionEnabled = false
           self.anyother_tf.isUserInteractionEnabled = false
            self.docName_tf.isUserInteractionEnabled = false
            self.docPhone_tf.isUserInteractionEnabled = false
            self.mediHistory_tf.isUserInteractionEnabled = false
            self.allergies_tf.isUserInteractionEnabled = false
            self.suburb_tf.isUserInteractionEnabled = false
            self.carername_tf.isUserInteractionEnabled = false
            self.carerphone_tf.isUserInteractionEnabled = false
            
            self.gender_tf.isUserInteractionEnabled = false
            self.secAddressline1_tf.isUserInteractionEnabled = false
            
            self.secsuburbn_tf.isUserInteractionEnabled = false
            self.secstate_tf.isUserInteractionEnabled = false
            self.secpostcode_tf.isUserInteractionEnabled = false
            self.carername_tf.isUserInteractionEnabled = false
            self.carerphone_tf.isUserInteractionEnabled = false
            self.email_tf.isUserInteractionEnabled = false
            self.mobile_tf.isUserInteractionEnabled = false

            
            
          gender_dropdown.isUserInteractionEnabled = false
           timedate_view.isHidden = true
                check_btn.isHidden = true

        
            let userData = UserDefaults.standard.value(forKey: "userData") as! NSDictionary
            self.name_lbl.text = userData.value(forKey: "name") as! String
            
           
        }
    override func viewWillAppear(_ animated: Bool) {
        
                ViewProfile()

    }
        
       
        // MARK: - Webservice Calling

        func  ViewProfile()
              {
                   let type = NVActivityIndicatorType.ballClipRotateMultiple
                                          let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                          let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                 self.view.addSubview(activityIndicatorView)
                                self.view.isUserInteractionEnabled = false
                   
                  
                       activityIndicatorView.startAnimating()
                       var param = [String:Any]()
           
                param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
             


                       WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                           
                           activityIndicatorView.stopAnimating()
                          self.view.isUserInteractionEnabled = true
                           
                           if let newResponse = response as? NSDictionary {
                               if newResponse.value(forKey: "status") as! Bool  == true {
                                
                                let userData = newResponse.value(forKey: "data") as! NSDictionary
                              
                                self.state_tf.text = userData.value(forKey: "state") as! String
                                
                                self.email_tf.text = userData.value(forKey: "email") as! String
                                self.mobile_tf.text = userData.value(forKey: "mobile") as! String
                                self.gender_tf.text = userData.value(forKey: "gender") as! String

                                self.secsuburbn_tf.text = userData.value(forKey: "secondary_suburb") as! String
                                self.secstate_tf.text = userData.value(forKey: "secondary_state") as! String
                                self.secpostcode_tf.text = userData.value(forKey: "secondary_postcode") as! String

                                self.docName_tf.text = userData.value(forKey: "doctor_name") as! String
                                self.docPhone_tf.text = userData.value(forKey: "doctor_phone") as! String

                                
                                
                                
                                
                                
                                          self.postcode_tf.text = userData.value(forKey: "postcode") as! String
                                          self.secAddressline1_tf.text = userData.value(forKey: "secondary_address") as! String
                                          self.name_lbl.text = userData.value(forKey: "name") as! String
                                          self.Dob_tf.text = userData.value(forKey: "dob") as! String
                                          self.addressLine1_tf.text = userData.value(forKey: "address1") as! String
                                          self.addressline2_tf.text = userData.value(forKey: "address2") as! String
//                                          self.mob_tf.text = userData.value(forKey: "mobile") as! String
//                                          self.phone_tf.text = userData.value(forKey: "phone") as! String
                                          self.mediCrdNumber_tf.text = userData.value(forKey: "medicare_card_no") as! String
                                          self.mediExpiry_tf.text = userData.value(forKey: "medicare_expiry_date") as! String
                                        self.concCrd_tf.text = userData.value(forKey: "concession_card_no") as! String
                                        self.consExpiry_tf.text = userData.value(forKey: "concession_expiry_date") as! String
                                         self.anyother_tf.text = userData.value(forKey: "other_entitlement") as! String
                                          self.docName_tf.text = userData.value(forKey: "doctor_name") as! String
                                          self.docPhone_tf.text = userData.value(forKey: "doctor_phone") as! String
                                          self.mediHistory_tf.text = userData.value(forKey: "medication_history") as! String
                                          self.allergies_tf.text = userData.value(forKey: "allergies") as! String
                                          self.suburb_tf.text = userData.value(forKey: "suburb") as! String
//                                          self.city_tf.text = userData.value(forKey: "city") as! String
                                self.name_lbl.text = userData.value(forKey: "name") as! String
                             
                                self.carerphone_tf.text = userData.value(forKey: "carer_phone_number") as! String
                                self.carername_tf.text = userData.value(forKey: "carer_name") as! String

                              
                            let image = userData.value(forKey: "image") as! String
                                
                     self.pro_img.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
                                
                                   }
                            else {
                                
                        self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                          }
                               }
                            
                           else {
                              self.view.showToast(toastMessage: "No Data Found", duration: 1)

                           }
                       }
                   
              }
        
        
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }
