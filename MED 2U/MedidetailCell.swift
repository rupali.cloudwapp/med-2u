//
//  MedidetailCell.swift
//  MED 2U
//
//  Created by CW-21 on 21/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

class MedidetailCell: UITableViewCell {

    @IBOutlet weak var detail_tv: UITextView!
    
    @IBOutlet weak var description_image: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
