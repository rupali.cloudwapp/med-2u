//
//  orderCell.swift
//  MED 2U
//
//  Created by CW-21 on 05/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

class orderCell: UITableViewCell {
    
    
    @IBOutlet weak var orderid_lbl: UILabel!
    
    
    @IBOutlet weak var status_lbl: UILabel!
    @IBOutlet weak var name_lbl: UILabel!
    
    @IBOutlet weak var address_lbl: UILabel!
    
    @IBOutlet weak var number_lbl: UILabel!
    
    @IBOutlet weak var info_btn: UIButton!
    
    
    @IBOutlet weak var viewinvoice_height: NSLayoutConstraint!
    @IBOutlet weak var viewDetail_btn: UIButton!
    
    @IBOutlet weak var viewinvoice_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
