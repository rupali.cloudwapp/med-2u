//
//  ShippingDetailsVC.swift
//  MED 2U
//
//  Created by CW-21 on 30/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import GooglePlaces
import NVActivityIndicatorView
import AlamofireImage
import Alamofire

@available(iOS 13.0, *)
class ShippingDetailsVC: UIViewController,GMSAutocompleteViewControllerDelegate  {

  class NetworkState {
        class func isConnected() ->Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }
       
    var appDele = UIApplication.shared.delegate as! AppDelegate
    
    
      //MARK : IBOUTLETS:
           
    @IBOutlet weak var success_popup: UIView!
    
           @IBOutlet weak var name_tf: UITextField!
           @IBOutlet weak var state_tf: UITextField!
                  
            @IBOutlet weak var postcode_tf: UITextField!
          @IBOutlet weak var secAddress_tf: UITextField!
         
           @IBOutlet weak var addressLine1_tf: UITextField!
         
           @IBOutlet weak var mob_tf: UITextField!
           
         
           @IBOutlet weak var city_tf: UITextField!
           @IBOutlet weak var signup_buton: DesignableButton!
  
           @IBOutlet weak var gradient_view: UIView!
          
    @IBOutlet weak var seccity_tf: UITextField!
    @IBOutlet weak var secstate_tf: UITextField!
    
    @IBOutlet weak var secpostcode_tf: UITextField!
    
    
    
    
    //MARK : IBACTIONS:
   
    @IBAction func ok_act(_ sender: Any) {
        

        if #available(iOS 13.0, *) { let scene = UIApplication.shared.connectedScenes.first
                                                        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                          sd.setHomeRootController()
                                                                                           }
                                                              }
                                              else
                                                                                           
                                  {
                                                                                                   
                      self.appDele.setHomeRootController()
                                                                                                   
                  }
        
    }
    
    @IBAction func BACK(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func secAddress2_act(_ sender: Any) {
        
        
        priAddressBool = false
               secAddressBool = true
               
               
               
                     let autocompleteController = GMSAutocompleteViewController()
                             autocompleteController.delegate = self
                             present(autocompleteController, animated: true, completion: nil)
        
        
        
        
        
    }
    
    
    
    @IBAction func address1_act(_ sender: Any) {
        
        priAddressBool = true
        secAddressBool = false
        
        
        
              let autocompleteController = GMSAutocompleteViewController()
                      autocompleteController.delegate = self
                      present(autocompleteController, animated: true, completion: nil)
          }
    
    @IBAction func signup_act(_ sender: Any) {
               
                
               if name_tf.text!.isEmpty {
                           
                           self.view.showToast(toastMessage:  "Name Required!!", duration: 1)
                           
               }
               else if addressLine1_tf.text!.isEmpty {
                      
                      self.view.showToast(toastMessage:  "Address Line1 Required!!", duration: 1)
                      
                      }
               else if state_tf.text!.isEmpty {
                             
                             self.view.showToast(toastMessage:  "Address Line2 Required!!", duration: 1)
                             
                             }
                 
                       else if city_tf.text!.isEmpty {
                                     
                                     self.view.showToast(toastMessage:  "City Required!!", duration: 1)
                                     
                                     }
                
                else if postcode_tf.text!.isEmpty {
                                                    
                                                    self.view.showToast(toastMessage:  "Postcode Required!!", duration: 1)
                                                    
                                                    }
                
                
               else if mob_tf.text!.isEmpty {
                                    
                    self.view.showToast(toastMessage:  "Mobile Required!!", duration: 1)
                                    
                                    }
            
         else
               {
                
//                if UserDefaults.standard.bool(forKey: "fromTxt") == true
//                {
                    OrdersToSubmitt()

//                }
//                else
//                {
//                     AddImgsOrder()
//                }
//
               }
           }
    
    
    //MARK : IBDECLARATIONS:
    var topassLat = String()
    var topassLong = String()
    
    
    var sectopassLat = String()
    var sectopassLong = String()
    
    var priAddressBool = Bool()
    var secAddressBool = Bool()

        var imgsArr = [UIImage]()
    var pahramcyIdStr = String()
    
    var dictionaries = [[String:Any]]()

    var addedorderMutableDict = [[String:Any]]()
    
    
    var forpostingorderMutabledict = [[String:Any]]()


   // var orderDetails =
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name_tf.textColor = .black
                         self.state_tf.textColor = .black
                         self.postcode_tf.textColor = .black
                  self.secAddress_tf.textColor = .black
                         self.addressLine1_tf.textColor = .black
                  self.mob_tf.textColor = .black
        
        self.city_tf.textColor = .black
               self.seccity_tf.textColor = .black
               self.secstate_tf.textColor = .black
        self.secpostcode_tf.textColor = .black
              
            
        
        if #available(iOS 13.0, *) {
                  let app = UIApplication.shared
                  let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                  
                  let statusbarView = UIView()
                  statusbarView.backgroundColor = themeColorFaint
                  view.addSubview(statusbarView)
                
                  statusbarView.translatesAutoresizingMaskIntoConstraints = false
                  statusbarView.heightAnchor
                      .constraint(equalToConstant: statusBarHeight).isActive = true
                  statusbarView.widthAnchor
                      .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                  statusbarView.topAnchor
                      .constraint(equalTo: view.topAnchor).isActive = true
                  statusbarView.centerXAnchor
                      .constraint(equalTo: view.centerXAnchor).isActive = true
                
              } else {
                  let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                  statusBar?.backgroundColor = themeColorFaint
              }
        
       /* for dict in addedorderMutableDict
        {
           print("dict",dict)
            
            
            
            let dictt = dict as NSDictionary
            print("dictt",dictt)

            
            let detail = dictt.object(forKey: "medicine_image")
            let description = dictt.object(forKey: "medicine_detail")

            let detl = detail as! Data
            
            let postdict = NSMutableDictionary()
            
//            postdict.addEntries(from: dictt as! [AnyHashable : Any])
//            postdict.setValue(detl.base, forKey: "medicine_image")
//            postdict.setValue(description, forKey: "medicine_detail")

            
            
//          dict.updateValue(detl.base64EncodedString(), forKey: "medicine_image")
//
//            dictt.up
//            updateValue(detl.base64EncodedString(), forKey: "medicine_image")
            
         //   dictt.setValue(detl.base64EncodedString(), forUndefinedKey: "medicine_image")
            
          //  ["medicine_image"] = detl.base64EncodedString()
            
            //setValue(detl.base64EncodedString(), forKeyPath: "medicine_image")
            
           


          //  setValue(detl.base64EncodedString(), forKey: "medicine_image")
            
    
           // updateValue( detail.base64EncodedString(), forKey: "medicine_image")
            print("postdict",postdict)
           
            forpostingorderMutabledict.append(postdict as! [String : Any])
           
               }
               
        print("forpostingorderMutabledict",forpostingorderMutabledict)*/
        
        
                //For setting grdaient :-
                                              
                                                     self.signup_buton.clipsToBounds = true
                                                     let gradientLayer: CAGradientLayer = CAGradientLayer()
                                                     gradientLayer.frame = view.bounds
                                                     let topColor: CGColor = themeColorFaint.cgColor
                                                     let middleColor: CGColor = themeColorDark.cgColor
                                                   let bottomColor: CGColor = themeColorDark.cgColor
                                                     gradientLayer.colors = [topColor, middleColor, bottomColor]
                                                     gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                                     gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                                                     self.signup_buton.layer.insertSublayer(gradientLayer, at: 0)
                
                self.gradient_view.clipsToBounds = true
                                                         
                                                          self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
                
                
        addressLine1_tf.isUserInteractionEnabled = false
        state_tf.isUserInteractionEnabled = false
        city_tf.isUserInteractionEnabled = false
        postcode_tf.isUserInteractionEnabled = false
        
        
        secAddress_tf.isUserInteractionEnabled = false
              secstate_tf.isUserInteractionEnabled = false
              seccity_tf.isUserInteractionEnabled = false
              secpostcode_tf.isUserInteractionEnabled = false

       ViewProfile()
        
        
    }
    
    
//    class func convertImageToBase64(image: UIImage) -> String {
//        let imageData = UIImagePNGRepresentation(image)!
//        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
//    }

    
    
 
    func OrdersToSubmitt()  {
       
         
let type = NVActivityIndicatorType.ballClipRotateMultiple
                                           let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                           let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
        activityIndicatorView.startAnimating()
                                  self.view.addSubview(activityIndicatorView)
                                 self.view.isUserInteractionEnabled = false
        
        let userid = UserDefaults.standard.value(forKey: "userIdLogin") as! String
        
        print("userid",userid)
        
        

       
        
     let myNewDictArray : [String : Any] =
                            [
                                "user_id" : userid,
                              "pharmecy_id" : pahramcyIdStr,
                              "name": self.name_tf.text as! String,
                              "mobile": self.mob_tf.text as! String,
                              "temp_mobile":self.mob_tf.text as! String,
                              "latitude": self.topassLat,
                              "longitude":self.topassLong,
                              "address1": self.addressLine1_tf.text as! String,
                              "address2":self.secAddress_tf.text as! String,
                              "secondary_address":self.secAddress_tf.text as! String,

                             "secondary_latitude": self.sectopassLat,
                              "secondary_longitude":self.sectopassLong,
                              "city": self.city_tf.text as! String,
                              "state": self.state_tf.text as! String,
                              "postcode": self.postcode_tf.text as! String,
                              "secondary_suburb": self.seccity_tf.text as! String,
                              "secondary_city": self.seccity_tf.text as! String,
                              "secondary_state": self.secstate_tf.text as! String,
                              "secondary_postcode": self.secpostcode_tf.text as! String,
  "order_detail": addedorderMutableDict

                            ]
        
        //print("myNewDictArray",myNewDictArray)
        
        
        WebService().postRequestJsonType(methodName: order_text, parameter: myNewDictArray) { (response) in
            //   SwiftLoader.hide()
               if let result = response as? NSDictionary {
                   if (result.value(forKey: "status") as! Bool) == true {
                      // if let newDict = result.value(forKey: "data") as? NSDictionary {
                           
                           
                          activityIndicatorView.stopAnimating()
            self.view.isUserInteractionEnabled = true
                    
                    
                     self.success_popup.isHidden = false
                                                                 
                                                                 
                           
                      // }
                   }
                   else {
                       activityIndicatorView.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                                                   print("failure")
                                                   self.view.showToast(toastMessage: "Failed To Place Order", duration: 1)                   }
               }
           }
        
        
     
               
        
    }
    
     // MARK: - Webservice Calling

            func  ViewProfile()
                  {
                       let type = NVActivityIndicatorType.ballClipRotateMultiple
                                              let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                              let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                     self.view.addSubview(activityIndicatorView)
                                    self.view.isUserInteractionEnabled = false
                       
                      
                           activityIndicatorView.startAnimating()
                           var param = [String:Any]()
               
                    param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                 


                           WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                            
                            
                            
                               
                               activityIndicatorView.stopAnimating()
                              self.view.isUserInteractionEnabled = true
                               
                               if let newResponse = response as? NSDictionary {
                                   if newResponse.value(forKey: "status") as! Bool  == true {
                                    
                                    let userData = newResponse.value(forKey: "data") as! NSDictionary
                                  
                                    self.state_tf.text = userData.value(forKey: "state") as! String
                                    
                                    self.mob_tf.text = userData.value(forKey: "mobile") as! String
                                  

                                    self.seccity_tf.text = userData.value(forKey: "secondary_suburb") as! String
                                    self.secstate_tf.text = userData.value(forKey: "secondary_state") as! String
                                    self.secpostcode_tf.text = userData.value(forKey: "secondary_postcode") as! String
  
                                    
                                              self.postcode_tf.text = userData.value(forKey: "postcode") as! String
                                              self.secAddress_tf.text = userData.value(forKey: "secondary_address") as! String
                                              self.name_tf.text = userData.value(forKey: "name") as! String
                                                   
                                              self.city_tf.text = userData.value(forKey: "suburb") as! String
   
                                     self.addressLine1_tf.text = userData.value(forKey: "address1") as! String
                                    
                                    
                                    let lat = userData.value(forKey: "latitude") as! String
                                    let long = userData.value(forKey: "longitude") as! String

                                    let seclat = userData.value(forKey: "secondary_latitude") as! String
                                    let seclong = userData.value(forKey: "secondary_longitude") as! String
                                    
                   if lat.contains(")")
                                    {
                                        self.topassLat = lat.replacingOccurrences(of: ")", with: "")
                                        
                                    }
                     else
                                    {
                                        self.topassLat = lat

                                    }
                                        
                 if long.contains(")")
                                                  {
                                                self.topassLong = long.replacingOccurrences(of: ")", with: "")
                                                }
                      else
                   {
                                         self.topassLong = long
                                                                         }
                                        
                                        
                         if seclat.contains(")")
                         {
                                          self.sectopassLat = seclat.replacingOccurrences(of: ")", with: "")
                                                                             
                                                                         }
                                                                         else
                                                                         {
                                                                             self.sectopassLat = seclat

                                                                         }
                    if seclong.contains(")")
                                    {
                                                    self.sectopassLong = seclong.replacingOccurrences(of: ")", with: "")
                                                                                     }
                                                                           else
                                                                      {
                                                                              self.sectopassLong = seclong
                                                                                                              }
                                        
                              
                                    
                                    
                                    
                                    
                                       }
                                else {
                                    
                            self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                              }
                                   }
                                
                               else {
                                  self.view.showToast(toastMessage: "No Data Found", duration: 1)

                               }
                           }
                       
                  }
            
    
    
    
    
    
    
    
    
    
    
    
    //MARK: CALING UPLOAD SERVICE:
       func AddImgsOrder() {
               let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                        let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                        let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                               self.view.addSubview(activityIndicatorView)
                                              self.view.isUserInteractionEnabled = false
           
           activityIndicatorView.startAnimating()
               
           var param = [String:Any]()
           
       let userid = UserDefaults.standard.value(forKey: "userIdLogin") as! String
        
       param["user_id"] = userid
           param["pharmecy_id"] = pahramcyIdStr
           param["name"] = self.name_tf.text as! String
        param["mobile"] = self.mob_tf.text as! String
           param["temp_mobile"] = self.mob_tf.text as! String
           param["latitude"] = self.topassLat
           param["longitude"] = self.topassLong
           param["address1"] = self.addressLine1_tf.text as! String
           param["address2"] = self.secAddress_tf.text as! String
           param["secondary_address"] = self.secAddress_tf.text as! String
           param["city"] = self.city_tf.text as! String
        param["state"] = self.state_tf.text as! String
           
           param["postcode"] = self.postcode_tf.text as! String
        
        param["secondary_suburb"] = self.seccity_tf.text as! String
              param["secondary_city"] = self.seccity_tf.text as! String
                 
                 param["secondary_state"] = self.secstate_tf.text as! String
        param["secondary_postcode"] = self.secpostcode_tf.text as! String
        param["secondary_latitude"] = self.sectopassLat as! String
        param["secondary_longitude"] = self.sectopassLong as! String


        
        
        
          
  
               var detailStr = String()
              
               
               if imgsArr.count != 0 {
                   var data = [Data]()
                   
                   print("inserted")
                   for img in imgsArr {
                       data.append((img.compressTo(1))!)
                   }
                   param["image[]"] = data
               }
               
            
               
           print("param",param)
            
           WebService().uploadImagesHandler(methodName: order_scan, parameter: param) { (result) in
               activityIndicatorView.stopAnimating()
            self.view.isUserInteractionEnabled = true
           if let dict = result as? NSDictionary {
           if dict.value(forKey: "status") as! Bool == true {

               self.view.showToast(toastMessage: "Order Placed Successfully", duration: 1)
            
            if #available(iOS 13.0, *) {
                                            let scene = UIApplication.shared.connectedScenes.first
                                                                                      if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                                                                                        sd.setHomeRootController()
                                                                                                                         }
                                                                                            }
                
                                                                            else
                                                                                                                         
                                                                {
                                                                                                                                 
                                                    self.appDele.setHomeRootController()
                                                                                                                                 
                                                }

           }
           else {
               
               self.view.showToast(toastMessage: (dict.value(forKey: "message") as? String)!, duration: 1)

           }
           }
           
           
       }
       }
    
    
    
    
    
    
    
    
    
    
    //MARK: GMSAUTOPICKER DELEGATE METHODS
                        
                        
                        func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
                            viewController.dismiss(animated: true, completion: nil)
                            
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress)")
                            print("Place attributions \(place.attributions)")
                            print("Place attributions \(place.coordinate.latitude))")
                            
                 //           UserDefaults.standard.setValue("\(String(describing: place.name))", forKey: "homeAddress")
                 //
                 //
                 //           UserDefaults.standard.setValue("\(place.coordinate.latitude))", forKey: "homeLat")
                 //           UserDefaults.standard.setValue("\(place.coordinate.longitude))", forKey: "homeLong")

                      var   locatioN = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                            
                            if priAddressBool == true
                            {
                            self.addressLine1_tf.text = "\(place.formattedAddress ?? "indore")"

                            self.topassLat = "\(place.coordinate.latitude)"
                            self.topassLong = "\(place.coordinate.longitude)"

                         getAddressFromLocation(location: locatioN)
                                
                                
                            }
                            
                            else
                            {
                                
                                self.secAddress_tf.text = "\(place.formattedAddress ?? "indore")"

                                self.sectopassLat = "\(place.coordinate.latitude)"
                               self.sectopassLong = "\(place.coordinate.longitude)"
                                
                                getAddressFromLocation(location: locatioN)

                            }
                            
                            
                            print("Place attributions \(place.coordinate.longitude))")
                         //   getCategory()

                            
                            
                        }
                        
                        func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
                            print("error")
                        }
                        
                        func wasCancelled(_ viewController: GMSAutocompleteViewController) {
                            
                            viewController.dismiss(animated: true, completion: nil)
                            
                            print("No place selected")
                        }
    
    
    
    
    
    func getAddressFromLocation(location: CLLocation) {
     
     

    
     CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
         print(location)
         
        
         if error != nil {
           return
         }

         var placeMark: CLPlacemark!
         placeMark = placemarks?[0]

         if placeMark == nil {
             return
         }

         if self.priAddressBool == true
                     {
         // City
         if let city = placeMark.addressDictionary!["City"] as? String {
             print(city, terminator: "")
             self.city_tf.text = city
         }

        
         
         // state
         if let state = placeMark.addressDictionary!["State"] as? String {
             self.state_tf.text = state
         }
         // Zip
         if let zip = placeMark.addressDictionary!["Zip"] as? String {
             self.postcode_tf.text = zip
          
         }
                       
                       let zipCode = placeMark.postalCode ?? "unknown"
                       self.postcode_tf.text = zipCode

                       
                       
                       
                       
         }
         
     else if self.secAddressBool == true
         
         {
             
             // City
                   if let city = placeMark.addressDictionary!["City"] as? String {
                       print(city, terminator: "")
                       self.seccity_tf.text = city
                   }

                  
                   
                   // state
                   if let state = placeMark.addressDictionary!["State"] as? String {
                       self.secstate_tf.text = state
                   }
                   // Zip
                   if let zip = placeMark.addressDictionary!["Zip"] as? String {
                       self.secpostcode_tf.text = zip
                   }
             let zipCode = placeMark.postalCode ?? "unknown"
                                  self.secpostcode_tf.text = zipCode
             
         }
         
         
         
         


     })

     }

    
    
    
    
    
    
    
              
              
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
