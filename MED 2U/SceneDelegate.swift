//
//  SceneDelegate.swift
//  Getmy color
//
//  Created by CW-21 on 23/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import SwiftUI

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var navigationController = UINavigationController()

     func setHomeRootController()  {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          var rootController = UIViewController()
          
          rootController = storyboard.instantiateViewController(withIdentifier: "CustomSideMenuController") as! CustomSideMenuController
          
          let navigationView = UINavigationController(rootViewController: rootController)
          navigationView.navigationBar.isHidden = true
          
          if let window = self.window {
              window.rootViewController = navigationView
          }
      }
    func setFirstRootController()  {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           var rootController = UIViewController()
           
           rootController = storyboard.instantiateViewController(withIdentifier: "FirstVC") as! FirstVC
           
           let navigationView = UINavigationController(rootViewController: rootController)
           navigationView.navigationBar.isHidden = true
           
           if let window = self.window {
               window.rootViewController = navigationView
           }
       }

    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
     
        if UserDefaults.standard.value(forKey: "userIdLogin") != nil
                   {
                       setHomeRootController()

                   }
                   else
                   {

                       setFirstRootController()
                   }
        
        
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

