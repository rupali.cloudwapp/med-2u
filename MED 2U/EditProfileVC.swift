//
//  EditProfileVC.swift
//  MED 2U
//
//  Created by CW-21 on 29/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import GooglePlaces
import NVActivityIndicatorView
import AlamofireImage
import MKDropdownMenu

@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource  {
    
    
    
    
    
    @IBOutlet weak var name_tf: UITextField!
    
    @IBOutlet weak var email_tf: UITextField!
     
     
     @IBOutlet weak var mobile_tf: UITextField!
    
     var tag = 0
        
        
        var nameStr = String()

      var genderArr = ["Male","Female"]
        
        var priAddressbool = Bool()
        var secAddressbool = Bool()

        var placesClient = GMSPlacesClient()
        
         //MARK : IBOUTLETS:
        
        
        @IBOutlet weak var gender_dropdown: MKDropdownMenu!
        
        @IBOutlet weak var gender_tf: UITextField!
        
        @IBOutlet weak var Dob_tf: UITextField!
        
        @IBOutlet weak var addressLine1_tf: UITextField!
        @IBOutlet weak var addressline2_tf: UITextField!
        
        
        @IBOutlet weak var state_tf: UITextField!
        
        @IBOutlet weak var postcode_tf: UITextField!
        
        
        @IBOutlet weak var secAddressline1_tf: UITextField!
        
        @IBOutlet weak var mediCrdNumber_tf: UITextField!
        @IBOutlet weak var mediExpiry_tf: UITextField!
        @IBOutlet weak var concCrd_tf: UITextField!
        @IBOutlet weak var consExpiry_tf: UITextField!
        @IBOutlet weak var anyother_tf: UITextField!
        @IBOutlet weak var docName_tf: UITextField!
        @IBOutlet weak var docPhone_tf: UITextField!
        @IBOutlet weak var mediHistory_tf: UITextField!
        @IBOutlet weak var allergies_tf: UITextField!
        @IBOutlet weak var suburb_tf: UITextField!
        @IBOutlet weak var name_lbl: UILabel!
        @IBOutlet weak var signup_buton: DesignableButton!

        @IBOutlet weak var pro_img: UIImageView!
        
        @IBOutlet weak var gradient_view: UIView!
        @IBOutlet weak var datePicker: UIDatePicker!

        @IBOutlet weak var timedate_view: UIView!
        
        
        @IBOutlet weak var secsuburbn_tf: UITextField!
        
        @IBOutlet weak var secstate_tf: UITextField!
        
        
        @IBOutlet weak var secpostcode_tf: UITextField!
        

        @IBOutlet weak var carername_tf: UITextField!
        
        @IBOutlet weak var carerphone_tf: UITextField!
        
        
        
        @IBOutlet weak var check_btn: UIButton!
        
        
        
        //MARK : IBACTIONS:
        
        @IBAction func secaddress1_act(_ sender: Any) {
            
            priAddressbool = false
           secAddressbool = true
            
            let autocompleteController = GMSAutocompleteViewController()
                          autocompleteController.delegate = self
                          present(autocompleteController, animated: true, completion: nil)
            
             
        }
        
        
        @IBAction func check_act(_ sender: Any) {
            if tag == 0
            {
                check_btn.isSelected = true
                tag = 1
            }
           else
            {
               check_btn.isSelected = false
                tag = 0
            }
            
            
            
        }
        
        
        
        
        
        
        
        @IBAction func dob_act(_ sender: Any) {
            mediExpirybool = false
                  consExpirybool = false
                  dobbool = true
         datePicker.datePickerMode = .date
          let minDays = Calendar.current.date(byAdding: .year, value: -100, to: Date())

            let maxDays = Calendar.current.date(byAdding: .year, value: -18, to: Date())
                datePicker?.minimumDate = minDays ?? Date()
               datePicker?.maximumDate = maxDays ?? Date()
            timedate_view.isHidden = false

        }
        @IBAction func done_act(_ sender: Any) {
            
            if mediExpirybool == true
            
            {
            
            mediExpiry_tf.text = (datePicker.date.dateToStr(format: "MM/yyyy"))
               
                
            }
                else if consExpirybool == true
                {
                    consExpiry_tf.text = (datePicker.date.dateToStr(format: "MM/yyyy"))

                }
            else
            {
                
                Dob_tf.text = (datePicker.date.dateToStr(format: "yyyy-MM-dd"))

            }
            
            
            timedate_view.isHidden = true


        }
        @IBAction func cancel_act(_ sender: Any) {
            
            timedate_view.isHidden = true
        }
        
        @IBAction func consExp_act(_ sender: Any) {
            mediExpirybool = false
                consExpirybool = true
            dobbool = false
            timedate_view.isHidden = false
                          datePicker.datePickerMode = .date
                                            let pastDays = Calendar.current.date(byAdding: .year, value: 100, to: Date())
                          datePicker?.minimumDate = Date()

                                                datePicker?.maximumDate = pastDays ?? Date()
        }
        
        @IBAction func mediExp_act(_ sender: Any) {
            mediExpirybool = true
            consExpirybool = false
            dobbool = false

            timedate_view.isHidden = false
                          datePicker.datePickerMode = .date
                                            let pastDays = Calendar.current.date(byAdding: .year, value: 100, to: Date())
                          datePicker?.minimumDate = Date()

                                                datePicker?.maximumDate = pastDays ?? Date()
        }
        @IBAction func address1_act(_ sender: Any) {
            
            priAddressbool = true
                  secAddressbool = false
            
            
            let autocompleteController = GMSAutocompleteViewController()
                    autocompleteController.delegate = self
                    present(autocompleteController, animated: true, completion: nil)
        }
        
        @IBAction func signup_act(_ sender: Any) {
            
            self.gender_dropdown.closeAllComponents(animated: true)

            
             if name_tf.text!.isEmpty {
                        
                        self.view.showToast(toastMessage:  "Name Required!!", duration: 1)
                        
                        }
            else if email_tf.text!.isEmpty {
                                   
                                   self.view.showToast(toastMessage:  "Email Required!!", duration: 1)
                                   
                                   }
           else  if mobile_tf.text!.isEmpty {
                       
                       self.view.showToast(toastMessage:  "Mobile Number Required!!", duration: 1)
                       
                       }
            
            if Dob_tf.text!.isEmpty {
            
            self.view.showToast(toastMessage:  "DOB Required!!", duration: 1)
            
            }
            else if addressLine1_tf.text!.isEmpty {
                   
                   self.view.showToast(toastMessage:  "Address Line1 Required!!", duration: 1)
                   
                   }
        
                else if suburb_tf.text!.isEmpty {
                           
                           self.view.showToast(toastMessage:  "Suburb/city Required!!", duration: 1)
                           
                           }
                    else if state_tf.text!.isEmpty {
                                  
                                  self.view.showToast(toastMessage:  "State Required!!", duration: 1)
                                  
                                  }
                else if postcode_tf.text!.isEmpty {
                                           
                           self.view.showToast(toastMessage:  "Postcode Required!!", duration: 1)
                                           
                                           }
           
      else
            {
     
           updateProfile()
            
            }
        }
        
        @IBAction func pro_act(_ sender: Any) {
            
            openFileAttachment ()
        }
        
        @IBAction func BACK(_ sender: Any) {
            
            self.gender_dropdown.closeAllComponents(animated: true)
                 self.navigationController?.popViewController(animated: true)
             }
        
        
       //MARK : IBDECLARATIONS:
        
        var appDele = UIApplication.shared.delegate as! AppDelegate

         var topassLat = String()
         var topassLong = String()
        var sectopassLat = String()
        var sectopassLong = String()
        
        var mediExpirybool = Bool()
          var consExpirybool = Bool()
        var dobbool = Bool()
          
        var imgStr = String()
              var emailStr = String()
                    var mobileStr = String()
                    var PwStr = String()
                 var stateStr = String()
                 var pstcodeStr = String()
                 var carenameStr = String()
                 var carephoneStr = String()
                 var secondAddressStr = String()
                 var doctorDetailsStr = String()
        
      let picker = UIImagePickerController()
        
        
        
        
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.gender_tf.textColor = .black
                             self.Dob_tf.textColor = .black
                             self.addressLine1_tf.textColor = .black
                      self.addressline2_tf.textColor = .black
                             self.state_tf.textColor = .black
                      self.postcode_tf.textColor = .black
                
                self.secAddressline1_tf.textColor = .black
                                    self.mediCrdNumber_tf.textColor = .black
                                    self.mediExpiry_tf.textColor = .black
                             self.concCrd_tf.textColor = .black
                                    self.consExpiry_tf.textColor = .black
                             self.anyother_tf.textColor = .black
                
                self.docName_tf.textColor = .black
                                           self.docPhone_tf.textColor = .black
                                           self.mediHistory_tf.textColor = .black
                                    self.allergies_tf.textColor = .black
                                           self.suburb_tf.textColor = .black
                                    self.secsuburbn_tf.textColor = .black
            
            if #available(iOS 13.0, *) {
                      let app = UIApplication.shared
                      let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                      
                      let statusbarView = UIView()
                      statusbarView.backgroundColor = themeColorFaint
                      view.addSubview(statusbarView)
                    
                      statusbarView.translatesAutoresizingMaskIntoConstraints = false
                      statusbarView.heightAnchor
                          .constraint(equalToConstant: statusBarHeight).isActive = true
                      statusbarView.widthAnchor
                          .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                      statusbarView.topAnchor
                          .constraint(equalTo: view.topAnchor).isActive = true
                      statusbarView.centerXAnchor
                          .constraint(equalTo: view.centerXAnchor).isActive = true
                    
                  } else {
                      let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                      statusBar?.backgroundColor = themeColorFaint
                  }
            
            self.carername_tf.keyboardType = .default

            gender_dropdown.delegate = self
            gender_dropdown.dataSource = self
            
            
            //For setting grdaient :-
                                   
                                          self.signup_buton.clipsToBounds = true
                                          let gradientLayer: CAGradientLayer = CAGradientLayer()
                                          gradientLayer.frame = view.bounds
                                          let topColor: CGColor = themeColorFaint.cgColor
                                          let middleColor: CGColor = themeColorDark.cgColor
                                        let bottomColor: CGColor = themeColorDark.cgColor
                                          gradientLayer.colors = [topColor, middleColor, bottomColor]
                                          gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                          gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
         //   self.signup_buton.layer.insertSublayer(gradientLayer, at: 0))
            
            self.signup_buton.applyGradient(colours: [themeColorFaint, themeColorDark])
            
            
            
            self.gradient_view.clipsToBounds = true
                                                     
                                                      self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
            
            
            self.name_lbl.isHidden = false
            
            picker.delegate = self
            
            
            
            self.signup_buton.isHidden = false
                   
                   
                   self.state_tf.isUserInteractionEnabled = false
                   self.postcode_tf.isUserInteractionEnabled = false
                   self.Dob_tf.isUserInteractionEnabled = true
                   self.addressLine1_tf.isUserInteractionEnabled = true
                   self.addressline2_tf.isUserInteractionEnabled = true
                 
                   self.mediCrdNumber_tf.isUserInteractionEnabled = true
                   self.mediExpiry_tf.isUserInteractionEnabled = true
                 self.concCrd_tf.isUserInteractionEnabled = true
                 self.consExpiry_tf.isUserInteractionEnabled = true
                  self.anyother_tf.isUserInteractionEnabled = true
                   self.docName_tf.isUserInteractionEnabled = true
                   self.docPhone_tf.isUserInteractionEnabled = true
                   self.mediHistory_tf.isUserInteractionEnabled = true
                   self.allergies_tf.isUserInteractionEnabled = true
                   self.suburb_tf.isUserInteractionEnabled = false
                   self.carername_tf.isUserInteractionEnabled = true
                   self.carerphone_tf.isUserInteractionEnabled = true
                   
                   self.gender_tf.isUserInteractionEnabled = false
                   self.secAddressline1_tf.isUserInteractionEnabled = true
                   
                   self.secsuburbn_tf.isUserInteractionEnabled = false
                   self.secstate_tf.isUserInteractionEnabled = false
                   self.secpostcode_tf.isUserInteractionEnabled = false
                   self.carername_tf.isUserInteractionEnabled = true
                   self.carerphone_tf.isUserInteractionEnabled = true
                   self.email_tf.isUserInteractionEnabled = false
                   self.mobile_tf.isUserInteractionEnabled = true
            self.name_tf.isUserInteractionEnabled = true

            
            
            
          ViewProfile()
               
        }
        //METHOD FOR MKDROPDOWN :

        
        func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
               return 1
           }
           
           func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
               return 2
           }
          func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
            
                 return genderArr[row]
            
         }
         
         func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
            
                 gender_tf.text = genderArr[row]
           
             gender_dropdown.closeAllComponents(animated: true)
         }
        
        
        //METHOD FOR IMAGE PICKING:
        
      
        func openFileAttachment () {
            self.view.endEditing(true)
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
                self.openCamera()
            }))
            actionSheet.addAction(UIAlertAction(title:"Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
                self.openGallery()
            }))
            
            actionSheet.addAction(UIAlertAction(title:"Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
        }
        
        
        func openCamera() {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                picker.allowsEditing = false
                picker.sourceType = .camera
                picker.cameraCaptureMode = .photo
                present(picker, animated: true, completion: nil)
            }
            else{
                let alert = UIAlertController(title: "Alert", message: "No Camera found in your Device", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        func openGallery() {
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            // present(picker, animated: true, completion: nil)
            
            self.present(picker, animated: true, completion: nil)
        }
        // MARK: - ImagePicker Method
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
               pro_img.image = pickedImage
                imgStr = "\(pickedImage)"
            }
            
           picker.dismiss(animated: true, completion: nil)
        }
        
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
        
        // MARK: - Webservice Calling

        func  updateProfile()
               {
                   let type = NVActivityIndicatorType.ballClipRotateMultiple
                             let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                             let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                    self.view.addSubview(activityIndicatorView)
                   self.view.isUserInteractionEnabled = false
                    
                   
                        activityIndicatorView.startAnimating()
                        var param = [String:Any]()
                
             
               param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                param["gender"] = gender_tf.text as! String

                param["name"] = self.name_tf.text as! String
                 param["email"] = self.email_tf.text as! String
                   param["mobile"] = self.mobile_tf.text as! String
                   param["phone"] = self.mobile_tf.text as! String
                param["dob"] = Dob_tf.text as! String
                   param["address2"] = addressLine1_tf.text as! String
                   param["address1"] = addressline2_tf.text as! String
                   param["latitude"] = topassLat
                   param["longitude"] = topassLong
                   param["suburb"] = suburb_tf.text as! String
                   param["city"] = suburb_tf.text as! String
                   param["state"] = state_tf.text as! String
                   param["postcode"] = postcode_tf.text as! String
                   param["medicare_card_no"] = mediCrdNumber_tf.text as! String
                   param["medicare_expiry_date"] = mediExpiry_tf.text as! String
                   param["medication_history"] =  mediHistory_tf.text as! String
                   param["allergies"] = allergies_tf.text as! String
                   param["carer_name"] = carername_tf.text as! String
                   param["carer_phone_number"] = carerphone_tf.text as! String
                   param["secondary_address"] = secAddressline1_tf.text as! String
                
                param["secondary_latitude"] =  self.sectopassLat
                param["secondary_longitude"] =  self.sectopassLong
                param["secondary_suburb"] = secsuburbn_tf.text as! String
                param["secondary_city"] = secsuburbn_tf.text as! String
                param["secondary_state"] = secstate_tf.text as! String
                param["secondary_postcode"] = secpostcode_tf.text as! String

                
                
                   param["doctor_name"] = docName_tf.text as! String
                   param["doctor_phone"] = docPhone_tf.text as! String
                   param["concession_card_no"] = concCrd_tf.text as! String
                   param["concession_expiry_date"] = consExpiry_tf.text as! String

                   param["other_entitlement"] = anyother_tf.text as! String

                   param["fcm_token"] = "abcd"
                   param["device_type"] = "IOS"
                param["device_id"] =  UserDefaults.standard.value(forKey: "decviceId") as! String
                
                   param["image"] = UIImageJPEGRepresentation(pro_img.image!, 1)!

                                    WebService().uploadImagesHandler(methodName: update_profile , parameter: param) { (response) in
                                         
                                        activityIndicatorView.stopAnimating()
                                    self.view.isUserInteractionEnabled = true


                                             if let newResponse = response as? NSDictionary {
                                    if newResponse.value(forKey: "status") as! Bool  == true {
                                        
                                        self.view.showToast(toastMessage: "Profile Updated Successfully", duration: 1)
                                        self.navigationController?.popViewController(animated: true)

                        
                                             }
                                                else
                                    {

                                        self.view.showToast(toastMessage: "Failed", duration: 1)

                                                }
                                             
                                        }
                                         
                                         else {
                                               self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                               
                                         }
                                     }
                                     }
                                     
                
        
        //MARK: GMSAUTOPICKER DELEGATE METHODS
                     
                     
                     func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
                         viewController.dismiss(animated: true, completion: nil)
                        
    //
    //                    DataService.instance.place = place
    //                    FillAddress(place: place)
    //                    fillAddressForm()
    //                    print(DataService.instance._address_line1)
    //                    print(DataService.instance._city)
    //                    print(DataService.instance._postalCode)
    //                    print(DataService.instance._state)
    //                    print(DataService.instance._country)
    //                    DataService.instance.addressLabel = place.formattedAddress
                          
                         
                         print("Place name \(place.name)")
                         print("Place address \(place.formattedAddress)")
                         print("Place attributions \(place.placeID)")
                         print("Place attributions \(place.coordinate.latitude))")
                        
                        
                     var   locatioN = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)

                        
                        
                 if priAddressbool == true
                 {
                    self.addressLine1_tf.text = "\(place.formattedAddress ?? "indore")"
                    
                         self.suburb_tf.text = "\(place.name ?? "indore")"
                               
                               
                               let formattedaddressComa = place.formattedAddress?.components(separatedBy: ",")
                                              let formattedaddressComaCount = formattedaddressComa?.count
                                                             
                                                             let index = formattedaddressComaCount! - 2
                                                             print("index",index)
                                                             

                                                             let formattedaddressState = formattedaddressComa![index]


                                                        self.state_tf.text = "\(formattedaddressState)"


               self.state_tf.text = "\(formattedaddressState)"

                     
                    self.topassLat = "\(place.coordinate.latitude)"
                    self.topassLong = "\(place.coordinate.longitude)"
                    
                    getAddressFromLocation(location: locatioN)

                    
                        }
                        else if secAddressbool == true
                 {
                    self.secAddressline1_tf.text = "\(place.formattedAddress ?? "indore")"
                    self.secsuburbn_tf.text = "\(place.name ?? "indore")"
                    
                    
                    let formattedaddressComa = place.formattedAddress?.components(separatedBy: ",")
                                   let formattedaddressComaCount = formattedaddressComa?.count
                                                  
                                                  let index = formattedaddressComaCount! - 2
                                                  print("index",index)
                                                  

                                                  let formattedaddressState = formattedaddressComa![index]


                                             self.secstate_tf.text = "\(formattedaddressState)"
                    
                    
                    
                    

                    self.sectopassLat = "\(place.coordinate.latitude)"
                    self.sectopassLong = "\(place.coordinate.longitude)"
                    
                    
                    getAddressFromLocation(location: locatioN)

                 

                        }
                        
                       
                        
                         
              //           UserDefaults.standard.setValue("\(String(describing: place.name))", forKey: "homeAddress")
              //
              //
              //           UserDefaults.standard.setValue("\(place.coordinate.latitude))", forKey: "homeLat")
              //           UserDefaults.standard.setValue("\(place.coordinate.longitude))", forKey: "homeLong")

                         
                         

                         
                         print("Place attributions \(place.coordinate.longitude))")
                      //   getCategory()

                         
                         
                     }
        
        
     
                     
                     func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
                         print("error")
                     }
                     
                     func wasCancelled(_ viewController: GMSAutocompleteViewController) {
                         
                         viewController.dismiss(animated: true, completion: nil)
                         
                         print("No place selected")
                     }
    
    
     func  ViewProfile()
                  {
                       let type = NVActivityIndicatorType.ballClipRotateMultiple
                                              let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                              let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                     self.view.addSubview(activityIndicatorView)
                                    self.view.isUserInteractionEnabled = false
                       
                      
                           activityIndicatorView.startAnimating()
                           var param = [String:Any]()
               
                    param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                 


                           WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                               
                               activityIndicatorView.stopAnimating()
                              self.view.isUserInteractionEnabled = true
                               
                               if let newResponse = response as? NSDictionary {
                                   if newResponse.value(forKey: "status") as! Bool  == true {
                                    
                                    let userData = newResponse.value(forKey: "data") as! NSDictionary
                                  
                                    self.state_tf.text = userData.value(forKey: "state") as! String
                                    
                                    self.email_tf.text = userData.value(forKey: "email") as! String
                                    self.mobile_tf.text = userData.value(forKey: "mobile") as! String
                                    self.gender_tf.text = userData.value(forKey: "gender") as! String

                                    self.secsuburbn_tf.text = userData.value(forKey: "secondary_suburb") as! String
                                    self.secstate_tf.text = userData.value(forKey: "secondary_state") as! String
                                    self.secpostcode_tf.text = userData.value(forKey: "secondary_postcode") as! String

                                    self.docName_tf.text = userData.value(forKey: "doctor_name") as! String
                                    self.docPhone_tf.text = userData.value(forKey: "doctor_phone") as! String

                                  self.topassLat = userData.value(forKey: "latitude") as! String
                                    
                                    self.topassLong = userData.value(forKey: "longitude") as! String
                                    self.sectopassLat = userData.value(forKey: "secondary_latitude") as! String
                                    self.sectopassLong = userData.value(forKey: "secondary_longitude") as! String

                                  
                                    
                                              self.postcode_tf.text = userData.value(forKey: "postcode") as! String
                                              self.secAddressline1_tf.text = userData.value(forKey: "secondary_address") as! String
                                              self.name_lbl.text = userData.value(forKey: "name") as! String
                                    
                                    self.name_tf.text = userData.value(forKey: "name") as! String
                                    
                                              self.Dob_tf.text = userData.value(forKey: "dob") as! String
                                              self.addressLine1_tf.text = userData.value(forKey: "address1") as! String
                                              self.addressline2_tf.text = userData.value(forKey: "address2") as! String
    //                                          self.mob_tf.text = userData.value(forKey: "mobile") as! String
    //                                          self.phone_tf.text = userData.value(forKey: "phone") as! String
                                              self.mediCrdNumber_tf.text = userData.value(forKey: "medicare_card_no") as! String
                                              self.mediExpiry_tf.text = userData.value(forKey: "medicare_expiry_date") as! String
                                            self.concCrd_tf.text = userData.value(forKey: "concession_card_no") as! String
                                            self.consExpiry_tf.text = userData.value(forKey: "concession_expiry_date") as! String
                                             self.anyother_tf.text = userData.value(forKey: "other_entitlement") as! String
                                              self.docName_tf.text = userData.value(forKey: "doctor_name") as! String
                                              self.docPhone_tf.text = userData.value(forKey: "doctor_phone") as! String
                                              self.mediHistory_tf.text = userData.value(forKey: "medication_history") as! String
                                              self.allergies_tf.text = userData.value(forKey: "allergies") as! String
                                              self.suburb_tf.text = userData.value(forKey: "suburb") as! String
    //                                          self.city_tf.text = userData.value(forKey: "city") as! String
                                    self.name_lbl.text = userData.value(forKey: "name") as! String
                                 
                                    self.carerphone_tf.text = userData.value(forKey: "carer_phone_number") as! String
                                    self.carername_tf.text = userData.value(forKey: "carer_name") as! String

                                  
                                let image = userData.value(forKey: "image") as! String
                                    
                         self.pro_img.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
                                    
                                       }
                                else {
                                    
                            self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                              }
                                   }
                                
                               else {
                                  self.view.showToast(toastMessage: "No Data Found", duration: 1)

                               }
                           }
                       
                  }
    
    
       
    func getAddressFromLocation(location: CLLocation) {
      
      

     
      CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
          print(location)
          
         
          if error != nil {
            return
          }

          var placeMark: CLPlacemark!
          placeMark = placemarks?[0]

          if placeMark == nil {
              return
          }

          if self.priAddressbool == true
                      {
          // City
          if let city = placeMark.addressDictionary!["City"] as? String {
              print(city, terminator: "")
              self.suburb_tf.text = city
          }

         
          
          // state
          if let state = placeMark.addressDictionary!["State"] as? String {
              self.state_tf.text = state
          }
          // Zip
          if let zip = placeMark.addressDictionary!["Zip"] as? String {
              self.postcode_tf.text = zip
           
          }
                        
                        let zipCode = placeMark.postalCode ?? "unknown"
                        self.postcode_tf.text = zipCode

                        
                        
                        
                        
          }
          
      else if self.secAddressbool == true
          
          {
              
              // City
                    if let city = placeMark.addressDictionary!["City"] as? String {
                        print(city, terminator: "")
                        self.secsuburbn_tf.text = city
                    }

                   
                    
                    // state
                    if let state = placeMark.addressDictionary!["State"] as? String {
                        self.secstate_tf.text = state
                    }
                    // Zip
                    if let zip = placeMark.addressDictionary!["Zip"] as? String {
                        self.secpostcode_tf.text = zip
                    }
              let zipCode = placeMark.postalCode ?? "unknown"
                                   self.secpostcode_tf.text = zipCode
              
          }
          
          
          
          


      })

      }
    
            
           
           
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }
